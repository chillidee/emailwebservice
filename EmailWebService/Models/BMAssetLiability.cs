﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using EmailWebService.Models;

namespace EmailWebService.Models
{
  public class BMAssetLiability
  {
    public Int32 BMAssetLiabilityID { get; set; }
    public Int32 ApplicantID { get; set; }
    public String NatureOfDebt { get; set; }
    public Boolean IsRealEstate { get; set; }
    public String CreditorName { get; set; }
    public String CreditorEmail1 { get; set; }
    public String CreditorEmail2 { get; set; }
    public String AccountLoanNumber { get; set; }
    public Nullable<Boolean> IsSecured { get; set; }
    public Nullable<Decimal> AssetEstResaleValue { get; set; }
    public Nullable<Decimal> CreditLimit { get; set; }
    public Nullable<Decimal> TotalAmountOwing { get; set; }
    public Nullable<Decimal> SurplusShortfall { get; set; }
    public Nullable<Decimal> MnthRepayment { get; set; }
    public Nullable<Boolean> ToBeIncluded { get; set; }
    public Nullable<Decimal> DebtAmt { get; set; }
    public String Joint { get; set; }
    public String Details { get; set; }
    public String CreditorContactName { get; set; }
    public Nullable<DateTime> DateEmailSentToCreditor { get; set; }
    public Nullable<Decimal> MonthlyPaymentToCreditorFromSurplus { get; set; }
    public Nullable<Decimal> Percentage { get; set; }
    public Nullable<Int16> MoratoriumTermSought { get; set; }
    public string PaymentCommencingMMMM { get; set; }
    public string PaymentCommencingYYYY { get; set; }


    public BMApplicant BMApplicant { get; set; }


    public BMAssetLiability(Int32 ID)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("SELECT * FROM BMAssetLiability WHERE BMAssetLiabilityID = " + ID.ToString() + "", conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {

          BMAssetLiabilityID = ID;
          ApplicantID = (dr["ApplicantID"] as Int32?) ?? 0;
          NatureOfDebt = Convert.ToString(dr["NatureOfDebt"]);
          IsRealEstate = (Boolean)dr["IsRealEstate"];
          CreditorName = Convert.ToString(dr["CreditorName"]);
          CreditorEmail1 = Convert.ToString(dr["CreditorEmail1"]);
          CreditorEmail2 = Convert.ToString(dr["CreditorEmail2"]);
          AccountLoanNumber = Convert.ToString(dr["AccountLoanNumber"]);
          IsSecured = (dr["IsSecured"] as Boolean?) ?? null;
          AssetEstResaleValue = (dr["AssetEstResaleValue"] as Decimal?) ?? null;
          CreditLimit = (dr["CreditLimit"] as Decimal?) ?? null;
          TotalAmountOwing = (dr["TotalAmountOwing"] as Decimal?) ?? null;
          SurplusShortfall = (dr["SurplusShortfall"] as Decimal?) ?? null;
          MnthRepayment = (dr["MnthRepayment"] as Decimal?) ?? null;
          ToBeIncluded = (dr["ToBeIncluded"] as Boolean?) ?? null;
          DebtAmt = (dr["DebtAmt"] as Decimal?) ?? null;
          Joint = Convert.ToString(dr["Joint"]);
          Details = Convert.ToString(dr["Details"]);
          CreditorContactName = Convert.ToString(dr["CreditorContactName"]);
          DateEmailSentToCreditor = (dr["DateEmailSentToCreditor"] as DateTime?) ?? null;
          MonthlyPaymentToCreditorFromSurplus = (dr["MonthlyPaymentToCreditorFromSurplus"] as Decimal?) ?? null;
          Percentage = (dr["Percentage"] as Decimal?) ?? null;
          MoratoriumTermSought = (dr["MoratoriumTermSought"] as Int16?) ?? null;
          PaymentCommencingMMMM = Convert.ToString(dr["PaymentCommencingMMMM"]);
          PaymentCommencingYYYY = Convert.ToString(dr["PaymentCommencingYYYY"]);

        }
      }
    }

    public void Save()
    {
      //building the UPDATE string...
      StringBuilder sql = new StringBuilder();
      sql.Append("UPDATE BMApplicant SET ");
      sql.Append("SequenceID = @SequenceID, ");
      sql.Append("CallID = @CallID, ");
      sql.Append("CreateDate = @CreateDate, ");
      sql.Append("LastModifiedDate = @LastModifiedDate, ");
      sql.Append("BMStatusID = @BMStatusID, ");
      sql.Append("StatusStartDate = @StatusStartDate, ");
      sql.Append("StatusExpiryDate = @StatusExpiryDate, ");
      sql.Append("StatusEnteredBy = @StatusEnteredBy, ");
      sql.Append("StatusDate = @StatusDate, ");
      sql.Append("StatusDetails = @StatusDetails, ");
      sql.Append("CompanyID = @CompanyID, ");
      sql.Append("Referral = @Referral, ");
      sql.Append("Title = @Title, ");
      sql.Append("Surname = @Surname, ");
      sql.Append("FirstName = @FirstName, ");
      sql.Append("MiddleName = @MiddleName, ");
      sql.Append("Nickname = @Nickname, ");
      sql.Append("TelephoneHome = @TelephoneHome, ");
      sql.Append("TelephoneWork = @TelephoneWork, ");
      sql.Append("Mobile = @Mobile, ");
      sql.Append("Fax = @Fax, ");
      sql.Append("Email = @Email, ");
      sql.Append("Email02 = @Email02, ");
      sql.Append("TelephoneHomePrefer = @TelephoneHomePrefer, ");
      sql.Append("TelephoneWorkPrefer = @TelephoneWorkPrefer, ");
      sql.Append("MobilePrefer = @MobilePrefer, ");
      sql.Append("SMSPrefer = @SMSPrefer, ");
      sql.Append("FaxPrefer = @FaxPrefer, ");
      sql.Append("EmailPrefer = @EmailPrefer, ");
      sql.Append("EmailSecondPrefer = @EmailSecondPrefer, ");
      sql.Append("PersonalRefName = @PersonalRefName, ");
      sql.Append("PersonalRefAddress = @PersonalRefAddress, ");
      sql.Append("PersonalRefRelationship = @PersonalRefRelationship, ");
      sql.Append("PersonalRefPhone = @PersonalRefPhone, ");
      sql.Append("Sex = @Sex, ");
      sql.Append("DateOfBirth = @DateOfBirth, ");
      sql.Append("HomeUnitNumber = @HomeUnitNumber, ");
      sql.Append("HomeStreetNumber = @HomeStreetNumber, ");
      sql.Append("HomeStreetName = @HomeStreetName, ");
      sql.Append("HomeStreetType = @HomeStreetType, ");
      sql.Append("HomeSuburb = @HomeSuburb, ");
      sql.Append("HomeState = @HomeState, ");
      sql.Append("PostalUnitNumber = @PostalUnitNumber, ");
      sql.Append("PostalStreetNumber = @PostalStreetNumber, ");
      sql.Append("PostalStreetName = @PostalStreetName, ");
      sql.Append("PostalStreetType = @PostalStreetType, ");
      sql.Append("PostalSuburb = @PostalSuburb, ");
      sql.Append("PostalState = @PostalState, ");
      sql.Append("PrevUnitNumber = @PrevUnitNumber, ");
      sql.Append("PrevStreetNumber = @PrevStreetNumber, ");
      sql.Append("PrevStreetName = @PrevStreetName, ");
      sql.Append("PrevStreetType = @PrevStreetType, ");
      sql.Append("PrevSuburb = @PrevSuburb, ");
      sql.Append("PrevState = @PrevState, ");
      sql.Append("HavingDriverLicense = @HavingDriverLicense, ");
      sql.Append("DriversLicenceNo = @DriversLicenceNo, ");
      sql.Append("LicenseStateOfIssue = @LicenseStateOfIssue, ");
      sql.Append("CurrentEmp1Name = @CurrentEmp1Name, ");
      sql.Append("CurrentEmp1AddressLine1 = @CurrentEmp1AddressLine1, ");
      sql.Append("CurrentEmp1AddressLine2 = @CurrentEmp1AddressLine2, ");
      sql.Append("CurrentEmp1lAs = @CurrentEmp1lAs, ");
      sql.Append("CurrentEmp1Industry = @CurrentEmp1Industry, ");
      sql.Append("CurrentEmp1PayPeriod = @CurrentEmp1PayPeriod, ");
      sql.Append("CurrentEmp1Since = @CurrentEmp1Since, ");
      sql.Append("CurrentEmp1Basis = @CurrentEmp1Basis, ");
      sql.Append("CurrentEmp1NetIncomeWeekly = @CurrentEmp1NetIncomeWeekly, ");
      sql.Append("CurrentEmp2Name = @CurrentEmp2Name, ");
      sql.Append("CurrentEmp2AddressLine1 = @CurrentEmp2AddressLine1, ");
      sql.Append("CurrentEmp2AddressLine2 = @CurrentEmp2AddressLine2, ");
      sql.Append("CurrentEmp2lAs = @CurrentEmp2lAs, ");
      sql.Append("CurrentEmp2Industry = @CurrentEmp2Industry, ");
      sql.Append("CurrentEmp2PayPeriod = @CurrentEmp2PayPeriod, ");
      sql.Append("CurrentEmp2Since = @CurrentEmp2Since, ");
      sql.Append("CurrentEmp2NetIncomeWeekly = @CurrentEmp2NetIncomeWeekly, ");
      sql.Append("CurrentEmp2Basis = @CurrentEmp2Basis, ");
      sql.Append("HavingSpouse = @HavingSpouse, ");
      sql.Append("Title_2 = @Title_2, ");
      sql.Append("Surname_2 = @Surname_2, ");
      sql.Append("FirstName_2 = @FirstName_2, ");
      sql.Append("MiddleName_2 = @MiddleName_2, ");
      sql.Append("Nickname_2 = @Nickname_2, ");
      sql.Append("TelephoneHome_2 = @TelephoneHome_2, ");
      sql.Append("TelephoneWork_2 = @TelephoneWork_2, ");
      sql.Append("Mobile_2 = @Mobile_2, ");
      sql.Append("Fax_2 = @Fax_2, ");
      sql.Append("Email_2 = @Email_2, ");
      sql.Append("Email02_2 = @Email02_2, ");
      sql.Append("TelephoneHomePrefer_2 = @TelephoneHomePrefer_2, ");
      sql.Append("TelephoneWorkPrefer_2 = @TelephoneWorkPrefer_2, ");
      sql.Append("MobilePrefer_2 = @MobilePrefer_2, ");
      sql.Append("SMSPrefer_2 = @SMSPrefer_2, ");
      sql.Append("FaxPrefer_2 = @FaxPrefer_2, ");
      sql.Append("EmailPrefer_2 = @EmailPrefer_2, ");
      sql.Append("EmailSecondPrefer_2 = @EmailSecondPrefer_2, ");
      sql.Append("PersonalRefName_2 = @PersonalRefName_2, ");
      sql.Append("PersonalRefAddress_2 = @PersonalRefAddress_2, ");
      sql.Append("PersonalRefRelationship_2 = @PersonalRefRelationship_2, ");
      sql.Append("PersonalRefPhone_2 = @PersonalRefPhone_2, ");
      sql.Append("Sex_2 = @Sex_2, ");
      sql.Append("DateOfBirth_2 = @DateOfBirth_2, ");
      sql.Append("HomeUnitNumber_2 = @HomeUnitNumber_2, ");
      sql.Append("HomeStreetNumber_2 = @HomeStreetNumber_2, ");
      sql.Append("HomeStreetName_2 = @HomeStreetName_2, ");
      sql.Append("HomeStreetType_2 = @HomeStreetType_2, ");
      sql.Append("HomeSuburb_2 = @HomeSuburb_2, ");
      sql.Append("HomeState_2 = @HomeState_2, ");
      sql.Append("PostalUnitNumber_2 = @PostalUnitNumber_2, ");
      sql.Append("PostalStreetNumber_2 = @PostalStreetNumber_2, ");
      sql.Append("PostalStreetName_2 = @PostalStreetName_2, ");
      sql.Append("PostalStreetType_2 = @PostalStreetType_2, ");
      sql.Append("PostalSuburb_2 = @PostalSuburb_2, ");
      sql.Append("PostalState_2 = @PostalState_2, ");
      sql.Append("PrevUnitNumber_2 = @PrevUnitNumber_2, ");
      sql.Append("PrevStreetNumber_2 = @PrevStreetNumber_2, ");
      sql.Append("PrevStreetName_2 = @PrevStreetName_2, ");
      sql.Append("PrevStreetType_2 = @PrevStreetType_2, ");
      sql.Append("PrevSuburb_2 = @PrevSuburb_2, ");
      sql.Append("PrevState_2 = @PrevState_2, ");
      sql.Append("HavingDriverLicense_2 = @HavingDriverLicense_2, ");
      sql.Append("DriversLicenceNo_2 = @DriversLicenceNo_2, ");
      sql.Append("LicenseStateOfIssue_2 = @LicenseStateOfIssue_2, ");
      sql.Append("CurrentEmp1Name_2 = @CurrentEmp1Name_2, ");
      sql.Append("CurrentEmp1AddressLine1_2 = @CurrentEmp1AddressLine1_2, ");
      sql.Append("CurrentEmp1AddressLine2_2 = @CurrentEmp1AddressLine2_2, ");
      sql.Append("CurrentEmp1lAs_2 = @CurrentEmp1lAs_2, ");
      sql.Append("CurrentEmp1Industry_2 = @CurrentEmp1Industry_2, ");
      sql.Append("CurrentEmp1PayPeriod_2 = @CurrentEmp1PayPeriod_2, ");
      sql.Append("CurrentEmp1Since_2 = @CurrentEmp1Since_2, ");
      sql.Append("CurrentEmp1Basis_2 = @CurrentEmp1Basis_2, ");
      sql.Append("CurrentEmp1NetIncomeWeekly_2 = @CurrentEmp1NetIncomeWeekly_2, ");
      sql.Append("CurrentEmp2Name_2 = @CurrentEmp2Name_2, ");
      sql.Append("CurrentEmp2AddressLine1_2 = @CurrentEmp2AddressLine1_2, ");
      sql.Append("CurrentEmp2AddressLine2_2 = @CurrentEmp2AddressLine2_2, ");
      sql.Append("CurrentEmp2lAs_2 = @CurrentEmp2lAs_2, ");
      sql.Append("CurrentEmp2Industry_2 = @CurrentEmp2Industry_2, ");
      sql.Append("CurrentEmp2PayPeriod_2 = @CurrentEmp2PayPeriod_2, ");
      sql.Append("CurrentEmp2Since_2 = @CurrentEmp2Since_2, ");
      sql.Append("CurrentEmp2NetIncomeWeekly_2 = @CurrentEmp2NetIncomeWeekly_2, ");
      sql.Append("CurrentEmp2Basis_2 = @CurrentEmp2Basis_2, ");
      sql.Append("NoOfDependents = @NoOfDependents, ");
      sql.Append("AgesOfDependents = @AgesOfDependents, ");
      sql.Append("LegalActionWithCreditor = @LegalActionWithCreditor, ");
      sql.Append("LegalActionCreditorName = @LegalActionCreditorName, ");
      sql.Append("FirstDifficultyPayingDebtYYYY = @FirstDifficultyPayingDebtYYYY, ");
      sql.Append("FirstDifficultyPayingDebtMMMM = @FirstDifficultyPayingDebtMMMM, ");
      sql.Append("FinDifficultyCauseID = @FinDifficultyCauseID, ");
      sql.Append("FinDifficultyCauseOther = @FinDifficultyCauseOther, ");
      sql.Append("ESBChangeToIncome12Month = @ESBChangeToIncome12Month, ");
      sql.Append("ESBChangeToIncome12MonthDesc = @ESBChangeToIncome12MonthDesc, ");
      sql.Append("PrevBankruptYear = @PrevBankruptYear, ");
      sql.Append("IncomeWageLast12M = @IncomeWageLast12M, ");
      sql.Append("IncomeGovBenefitLast12M = @IncomeGovBenefitLast12M, ");
      sql.Append("IncomeBusinessLast12M = @IncomeBusinessLast12M, ");
      sql.Append("IncomeTrustLast12M = @IncomeTrustLast12M, ");
      sql.Append("IncomeDividentLast12M = @IncomeDividentLast12M, ");
      sql.Append("IncomeOtherLast12M = @IncomeOtherLast12M, ");
      sql.Append("IncomeReceiveFromGov = @IncomeReceiveFromGov, ");
      sql.Append("IncomeReceiveFromBusiness = @IncomeReceiveFromBusiness, ");
      sql.Append("IncomeReceiveFromTrust = @IncomeReceiveFromTrust, ");
      sql.Append("IncomeReceiveFromDivident = @IncomeReceiveFromDivident, ");
      sql.Append("IncomeReceiveFromOther = @IncomeReceiveFromOther, ");
      sql.Append("PartnerNetWeeklyIncome = @PartnerNetWeeklyIncome, ");
      sql.Append("TotalIncomeAfterTax = @TotalIncomeAfterTax, ");
      sql.Append("TotalIncomeWithPartnerAfterTax = @TotalIncomeWithPartnerAfterTax, ");
      sql.Append("AgreedActualSurplus = @AgreedActualSurplus, ");
      sql.Append("Notes = @Notes, ");
      sql.Append("ApplicationManager = @ApplicationManager, ");
      sql.Append("Operator = @Operator, ");
      sql.Append("FileLocation = @FileLocation, ");
      sql.Append("SalesPackNumber = @SalesPackNumber, ");
      sql.Append("ApplicationFee = @ApplicationFee, ");
      sql.Append("MonthlyManagementFee = @MonthlyManagementFee, ");
      sql.Append("TotalBudgetToManage = @TotalBudgetToManage, ");

      sql.Append("WHERE ApplicantID = @ApplicantID");

      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

        cmd.Parameters.Add(new SqlParameter("BMAssetLiabilityID", ((object)BMAssetLiabilityID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicantID", ((object)ApplicantID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("NatureOfDebt", ((object)NatureOfDebt ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IsRealEstate", ((object)IsRealEstate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreditorName", ((object)CreditorName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreditorEmail1", ((object)CreditorEmail1 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreditorEmail2", ((object)CreditorEmail2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AccountLoanNumber", ((object)AccountLoanNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IsSecured", ((object)IsSecured ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AssetEstResaleValue", ((object)AssetEstResaleValue ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreditLimit", ((object)CreditLimit ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TotalAmountOwing", ((object)TotalAmountOwing ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("SurplusShortfall", ((object)SurplusShortfall ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("MnthRepayment", ((object)MnthRepayment ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ToBeIncluded", ((object)ToBeIncluded ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DebtAmt", ((object)DebtAmt ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Joint", ((object)Joint ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Details", ((object)Details ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreditorContactName", ((object)CreditorContactName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DateEmailSentToCreditor", ((object)DateEmailSentToCreditor ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("MonthlyPaymentToCreditorFromSurplus", ((object)MonthlyPaymentToCreditorFromSurplus ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Percentage", ((object)Percentage ?? DBNull.Value)));


        conn.Open();
        cmd.ExecuteNonQuery();
      }
    }
    public void LoadBMApplicant()
    {
      BMApplicant = new BMApplicant(Convert.ToInt32(ApplicantID));
    }

    public string NamesOfAccount
    {
      get
      {
        string s = string.Empty;
        if (BMApplicant == null) LoadBMApplicant();
        switch (Joint)
        {
          case "Main":
            s = (BMApplicant.FirstName + " " + BMApplicant.Surname).Trim();
            break;
          case "Partner":
            s = (BMApplicant.FirstName_2 + " " + BMApplicant.Surname_2).Trim();
            break;
          case "Joint":
            s = (BMApplicant.FirstName + " " + BMApplicant.Surname).Trim() + " and " + (BMApplicant.FirstName_2 + " " + BMApplicant.Surname_2).Trim();
            break;
          default:
            s = "*** Joint data not specified ***";
            break;
        }
        return s;
      }
    }

    public void LogContact(string ContactMadeBy, string Caption, string Notes, Nullable<Int32> EventDefID)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("INSERT INTO BMContactLog (BMAssetLiabilityID, ContactMadeBy, ContactDate, Caption, Notes, BMEventDefID) VALUES (@1,@2,@3,@4,@5,@6)");
        cmd.Parameters.Add(new SqlParameter("@1", BMAssetLiabilityID.ToString()));
        cmd.Parameters.Add(new SqlParameter("@2", ContactMadeBy));
        cmd.Parameters.Add(new SqlParameter("@3", DateTime.Now));
        cmd.Parameters.Add(new SqlParameter("@4", Caption));
        cmd.Parameters.Add(new SqlParameter("@5", Notes));
        cmd.Parameters.Add(new SqlParameter("@6", (object)EventDefID ?? DBNull.Value));
        
        conn.Open();
        cmd.Connection = conn;
        cmd.ExecuteNonQuery( );
      }

    }
  }
}