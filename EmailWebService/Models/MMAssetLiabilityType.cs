﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EmailWebService.Models
{
	public class MMAssetLiabilityType
	{
		public int AssetLiabilityID { get; set; }
		public string AssetLiability { get; set; }
		public string CitiBankALDesc { get; set; }
		public string Category { get; set; }
		public Nullable<short> SortOrder { get; set; }
		public bool DisplayOnWeb { get; set; }
		public int NumberOfDefaultCreates { get; set; }
		public bool IsCreditCard { get; set; }
		public bool IsRealEstate { get; set; }
		public bool ShowOwing { get; set; }
		public bool ShowValue { get; set; }

		public MMAssetLiabilityType() { }

		public MMAssetLiabilityType(int ID)
		{
			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM AssetLiabilityType WHERE AssetLiabilityID = " + ID.ToString(), conn);
				conn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read())
				{
					AssetLiabilityID = ID;
					
					AssetLiability = Convert.ToString(dr["AssetLiability"]);
					CitiBankALDesc = Convert.ToString(dr["CitiBankALDesc"]);
					Category = Convert.ToString(dr["Category"]);
					SortOrder = (dr["SortOrder"] as Int16?) ?? 0;
					DisplayOnWeb = (Boolean)dr["DisplayOnWeb"];
					NumberOfDefaultCreates = (dr["NumberOfDefaultCreates"] as Int32?) ?? 0;
					IsCreditCard = (Boolean)dr["IsCreditCard"];
					IsRealEstate = (Boolean)dr["IsRealEstate"];
					ShowOwing = (Boolean)dr["ShowOwing"];
					ShowValue = (Boolean)dr["ShowValue"];

				}
			}
		}
	}
}