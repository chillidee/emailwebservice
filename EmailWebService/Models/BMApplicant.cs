﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using EmailWebService.Models;

namespace EmailWebService.Models
{
  public class BMApplicant
  {
    private string _html_debt_table;
    private string _html_debt_table_mini;
    private string _html_debt_table_micro;
    private string _CheckList_Outstanding;
    private Decimal _TotalBudgetToManage = 0;
    private Decimal _TotalMonthlyPaymentToCreditorFromSurplus = 0;

    public Int32 ApplicantID { get; set; }
    public String GUID { get; set; }      
    public String SequenceID { get; set; }
    public Nullable<Int32> CallID { get; set; }
    public Nullable<DateTime> CreateDate { get; set; }
    public Nullable<DateTime> LastModifiedDate { get; set; }
    public Int32 BMStatusID { get; set; }
    public Nullable<DateTime> StatusStartDate { get; set; }
    public Nullable<DateTime> StatusExpiryDate { get; set; }
    public String StatusEnteredBy { get; set; }
    public Nullable<DateTime> StatusDate { get; set; }
    public String StatusDetails { get; set; }
    public Nullable<Int32> CompanyID { get; set; }
    public String Referral { get; set; }
    public String Title { get; set; }
    public String Surname { get; set; }
    public String FirstName { get; set; }
    public String MiddleName { get; set; }
    public String Nickname { get; set; }
    public String TelephoneHome { get; set; }
    public String TelephoneWork { get; set; }
    public String Mobile { get; set; }
    public String Fax { get; set; }
    public String Email { get; set; }
    public String Email02 { get; set; }
    public Boolean TelephoneHomePrefer { get; set; }
    public Boolean TelephoneWorkPrefer { get; set; }
    public Boolean MobilePrefer { get; set; }
    public Boolean SMSPrefer { get; set; }
    public Boolean FaxPrefer { get; set; }
    public Boolean EmailPrefer { get; set; }
    public Boolean EmailSecondPrefer { get; set; }
    public String PersonalRefName { get; set; }
    public String PersonalRefAddress { get; set; }
    public String PersonalRefRelationship { get; set; }
    public String PersonalRefPhone { get; set; }
    public String Sex { get; set; }
    public Nullable<DateTime> DateOfBirth { get; set; }
    public String HomeUnitNumber { get; set; }
    public String HomeStreetNumber { get; set; }
    public String HomeStreetName { get; set; }
    public String HomeStreetType { get; set; }
    public Nullable<Int32> HomeSuburb { get; set; }
    public String HomeState { get; set; }
    public String PostalUnitNumber { get; set; }
    public String PostalStreetNumber { get; set; }
    public String PostalStreetName { get; set; }
    public String PostalStreetType { get; set; }
    public Nullable<Int32> PostalSuburb { get; set; }
    public String PostalState { get; set; }
    public String PrevUnitNumber { get; set; }
    public String PrevStreetNumber { get; set; }
    public String PrevStreetName { get; set; }
    public String PrevStreetType { get; set; }
    public Nullable<Int32> PrevSuburb { get; set; }
    public String PrevState { get; set; }
    public Boolean HavingDriverLicense { get; set; }
    public String DriversLicenceNo { get; set; }
    public String LicenseStateOfIssue { get; set; }
    public String CurrentEmp1Name { get; set; }
    public String CurrentEmp1AddressLine1 { get; set; }
    public String CurrentEmp1AddressLine2 { get; set; }
    public String CurrentEmp1lAs { get; set; }
    public String CurrentEmp1Industry { get; set; }
    public String CurrentEmp1PayPeriod { get; set; }
    public Nullable<DateTime> CurrentEmp1Since { get; set; }
    public String CurrentEmp1Basis { get; set; }
    public Nullable<Decimal> CurrentEmp1NetIncomeWeekly { get; set; }
    public String CurrentEmp2Name { get; set; }
    public String CurrentEmp2AddressLine1 { get; set; }
    public String CurrentEmp2AddressLine2 { get; set; }
    public String CurrentEmp2lAs { get; set; }
    public String CurrentEmp2Industry { get; set; }
    public String CurrentEmp2PayPeriod { get; set; }
    public Nullable<DateTime> CurrentEmp2Since { get; set; }
    public Nullable<Decimal> CurrentEmp2NetIncomeWeekly { get; set; }
    public String CurrentEmp2Basis { get; set; }
    public Boolean HavingSpouse { get; set; }
    public String Title_2 { get; set; }
    public String Surname_2 { get; set; }
    public String FirstName_2 { get; set; }
    public String MiddleName_2 { get; set; }
    public String Nickname_2 { get; set; }
    public String TelephoneHome_2 { get; set; }
    public String TelephoneWork_2 { get; set; }
    public String Mobile_2 { get; set; }
    public String Fax_2 { get; set; }
    public String Email_2 { get; set; }
    public String Email02_2 { get; set; }
    public Boolean TelephoneHomePrefer_2 { get; set; }
    public Boolean TelephoneWorkPrefer_2 { get; set; }
    public Boolean MobilePrefer_2 { get; set; }
    public Boolean SMSPrefer_2 { get; set; }
    public Boolean FaxPrefer_2 { get; set; }
    public Boolean EmailPrefer_2 { get; set; }
    public Boolean EmailSecondPrefer_2 { get; set; }
    public String PersonalRefName_2 { get; set; }
    public String PersonalRefAddress_2 { get; set; }
    public String PersonalRefRelationship_2 { get; set; }
    public String PersonalRefPhone_2 { get; set; }
    public String Sex_2 { get; set; }
    public Nullable<DateTime> DateOfBirth_2 { get; set; }
    public String HomeUnitNumber_2 { get; set; }
    public String HomeStreetNumber_2 { get; set; }
    public String HomeStreetName_2 { get; set; }
    public String HomeStreetType_2 { get; set; }
    public Nullable<Int32> HomeSuburb_2 { get; set; }
    public String HomeState_2 { get; set; }
    public String PostalUnitNumber_2 { get; set; }
    public String PostalStreetNumber_2 { get; set; }
    public String PostalStreetName_2 { get; set; }
    public String PostalStreetType_2 { get; set; }
    public Nullable<Int32> PostalSuburb_2 { get; set; }
    public String PostalState_2 { get; set; }
    public String PrevUnitNumber_2 { get; set; }
    public String PrevStreetNumber_2 { get; set; }
    public String PrevStreetName_2 { get; set; }
    public String PrevStreetType_2 { get; set; }
    public Nullable<Int32> PrevSuburb_2 { get; set; }
    public String PrevState_2 { get; set; }
    public Boolean HavingDriverLicense_2 { get; set; }
    public String DriversLicenceNo_2 { get; set; }
    public String LicenseStateOfIssue_2 { get; set; }
    public String CurrentEmp1Name_2 { get; set; }
    public String CurrentEmp1AddressLine1_2 { get; set; }
    public String CurrentEmp1AddressLine2_2 { get; set; }
    public String CurrentEmp1lAs_2 { get; set; }
    public String CurrentEmp1Industry_2 { get; set; }
    public String CurrentEmp1PayPeriod_2 { get; set; }
    public Nullable<DateTime> CurrentEmp1Since_2 { get; set; }
    public String CurrentEmp1Basis_2 { get; set; }
    public Nullable<Decimal> CurrentEmp1NetIncomeWeekly_2 { get; set; }
    public String CurrentEmp2Name_2 { get; set; }
    public String CurrentEmp2AddressLine1_2 { get; set; }
    public String CurrentEmp2AddressLine2_2 { get; set; }
    public String CurrentEmp2lAs_2 { get; set; }
    public String CurrentEmp2Industry_2 { get; set; }
    public String CurrentEmp2PayPeriod_2 { get; set; }
    public Nullable<DateTime> CurrentEmp2Since_2 { get; set; }
    public Nullable<Decimal> CurrentEmp2NetIncomeWeekly_2 { get; set; }
    public String CurrentEmp2Basis_2 { get; set; }
    public Nullable<Int32> NoOfDependents { get; set; }
    public String AgesOfDependents { get; set; }
    public Boolean LegalActionWithCreditor { get; set; }
    public String LegalActionCreditorName { get; set; }
    public String FirstDifficultyPayingDebtYYYY { get; set; }
    public String FirstDifficultyPayingDebtMMMM { get; set; }
    public Nullable<Int32> FinDifficultyCauseID { get; set; }
    public String FinDifficultyCauseOther { get; set; }
    public Boolean ESBChangeToIncome12Month { get; set; }
    public String ESBChangeToIncome12MonthDesc { get; set; }
    public String PrevBankruptYear { get; set; }
    public Nullable<Decimal> IncomeWageLast12M { get; set; }
    public Nullable<Decimal> IncomeGovBenefitLast12M { get; set; }
    public Nullable<Decimal> IncomeBusinessLast12M { get; set; }
    public Nullable<Decimal> IncomeTrustLast12M { get; set; }
    public Nullable<Decimal> IncomeDividentLast12M { get; set; }
    public Nullable<Decimal> IncomeOtherLast12M { get; set; }
    public String IncomeReceiveFromGov { get; set; }
    public String IncomeReceiveFromBusiness { get; set; }
    public String IncomeReceiveFromTrust { get; set; }
    public String IncomeReceiveFromDivident { get; set; }
    public String IncomeReceiveFromOther { get; set; }
    public Nullable<Decimal> PartnerNetWeeklyIncome { get; set; }
    public Nullable<Decimal> TotalIncomeAfterTax { get; set; }
    public Nullable<Decimal> TotalIncomeWithPartnerAfterTax { get; set; }
    public Nullable<Decimal> AgreedActualSurplus { get; set; }
    public String Notes { get; set; }
    public String ApplicationManager { get; set; }
    public String Operator { get; set; }
    public String FileLocation { get; set; }
    public Nullable<Byte> SalesPackNumber { get; set; }
    public Nullable<Decimal> ApplicationFee { get; set; }
    public Nullable<Decimal> MonthlyManagementFee { get; set; }
    //public Nullable<Decimal> TotalBudgetToManage { get; set; }

    public Nullable<DateTime> PaymentFromClientStartDate { get; set; }
    public Nullable<Decimal> AgreedAmountToWithdrawFromClient { get; set; }
    public String WithdrawalFrequency { get; set; }
    //=[SumTotalAmountOwing]/[SumMonthlyPaymentToCreditorFromSurplus]

    public string FullAddress
    {
      get 
      {
        return (HomeUnitNumber + "/" + HomeStreetNumber + ", " + HomeStreetName + " " + HomeStreetType + ", " + new MMPCode(HomeSuburb).SuburbStatePostCode);
      }
    }

    public Decimal TotalBudgetToManage
    {
      get
      {                
        if (_TotalBudgetToManage == 0)
        {
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            SqlCommand cmd = new SqlCommand("SELECT Sum(TotalAmountOwing), Sum(MonthlyPaymentToCreditorFromSurplus_ACTUAL) FROM BMAssetLiability WHERE ApplicantID = " + ApplicantID.ToString() + " AND ToBeIncluded = 1", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
              while (dr.Read())
              { _TotalBudgetToManage = (Decimal)dr[0]; _TotalMonthlyPaymentToCreditorFromSurplus = (Decimal)dr[1]; }
            }
            else { _TotalBudgetToManage = 0; }

          }
        }        
        return _TotalBudgetToManage;
      }
    }

    public Decimal TotalMonthlyPaymentToCreditorFromSurplus
    {
      get
      {
        if (_TotalMonthlyPaymentToCreditorFromSurplus == 0)
        {
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            SqlCommand cmd = new SqlCommand("SELECT Sum(TotalAmountOwing), Sum(MonthlyPaymentToCreditorFromSurplus_ACTUAL) FROM BMAssetLiability WHERE ApplicantID = " + ApplicantID.ToString() + " AND ToBeIncluded = 1", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
              while (dr.Read())
              { _TotalBudgetToManage = (Decimal)dr[0]; _TotalMonthlyPaymentToCreditorFromSurplus = (Decimal)dr[1]; }
            }
            else { _TotalMonthlyPaymentToCreditorFromSurplus = 0; }

          }
        }
        return _TotalMonthlyPaymentToCreditorFromSurplus;
      }
    }

    public decimal NoOfMonths
    {
      get {
        Decimal r = 0;
        if (TotalMonthlyPaymentToCreditorFromSurplus != 0)
        { r = TotalBudgetToManage / TotalMonthlyPaymentToCreditorFromSurplus; }
        return r;
      }
    }

    public MMCompany Company { get; set; }

    public BMApplicant() { }

    public BMApplicant(Int32 ID)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("SELECT * FROM BMApplicant WHERE ApplicantID = " + ID.ToString() + "", conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {          

          ApplicantID = ID;
          GUID = Convert.ToString(dr["GUID"]);
          SequenceID = Convert.ToString(dr["SequenceID"]);
          CallID = (dr["CallID"] as Int32?) ?? null;
          CreateDate = (dr["CreateDate"] as DateTime?) ?? null;
          LastModifiedDate = (dr["LastModifiedDate"] as DateTime?) ?? null;
          BMStatusID = (dr["BMStatusID"] as Int32?) ?? 0;
          StatusStartDate = (dr["StatusStartDate"] as DateTime?) ?? null;
          StatusExpiryDate = (dr["StatusExpiryDate"] as DateTime?) ?? null;
          StatusEnteredBy = Convert.ToString(dr["StatusEnteredBy"]);
          StatusDate = (dr["StatusDate"] as DateTime?) ?? null;
          StatusDetails = Convert.ToString(dr["StatusDetails"]);
          CompanyID = (dr["CompanyID"] as Int32?) ?? null;
          Referral = Convert.ToString(dr["Referral"]);
          Title = Convert.ToString(dr["Title"]);
          Surname = Convert.ToString(dr["Surname"]);
          FirstName = Convert.ToString(dr["FirstName"]);
          MiddleName = Convert.ToString(dr["MiddleName"]);
          Nickname = Convert.ToString(dr["Nickname"]);
          TelephoneHome = Convert.ToString(dr["TelephoneHome"]);
          TelephoneWork = Convert.ToString(dr["TelephoneWork"]);
          Mobile = Convert.ToString(dr["Mobile"]);
          Fax = Convert.ToString(dr["Fax"]);
          Email = Convert.ToString(dr["Email"]);
          Email02 = Convert.ToString(dr["Email02"]);
          TelephoneHomePrefer = (Boolean)dr["TelephoneHomePrefer"];
          TelephoneWorkPrefer = (Boolean)dr["TelephoneWorkPrefer"];
          MobilePrefer = (Boolean)dr["MobilePrefer"];
          SMSPrefer = (Boolean)dr["SMSPrefer"];
          FaxPrefer = (Boolean)dr["FaxPrefer"];
          EmailPrefer = (Boolean)dr["EmailPrefer"];
          EmailSecondPrefer = (Boolean)dr["EmailSecondPrefer"];
          PersonalRefName = Convert.ToString(dr["PersonalRefName"]);
          PersonalRefAddress = Convert.ToString(dr["PersonalRefAddress"]);
          PersonalRefRelationship = Convert.ToString(dr["PersonalRefRelationship"]);
          PersonalRefPhone = Convert.ToString(dr["PersonalRefPhone"]);
          Sex = Convert.ToString(dr["Sex"]);
          DateOfBirth = (dr["DateOfBirth"] as DateTime?) ?? null;
          HomeUnitNumber = Convert.ToString(dr["HomeUnitNumber"]);
          HomeStreetNumber = Convert.ToString(dr["HomeStreetNumber"]);
          HomeStreetName = Convert.ToString(dr["HomeStreetName"]);
          HomeStreetType = Convert.ToString(dr["HomeStreetType"]);
          HomeSuburb = (dr["HomeSuburb"] as Int32?) ?? null;
          HomeState = Convert.ToString(dr["HomeState"]);
          PostalUnitNumber = Convert.ToString(dr["PostalUnitNumber"]);
          PostalStreetNumber = Convert.ToString(dr["PostalStreetNumber"]);
          PostalStreetName = Convert.ToString(dr["PostalStreetName"]);
          PostalStreetType = Convert.ToString(dr["PostalStreetType"]);
          PostalSuburb = (dr["PostalSuburb"] as Int32?) ?? null;
          PostalState = Convert.ToString(dr["PostalState"]);
          PrevUnitNumber = Convert.ToString(dr["PrevUnitNumber"]);
          PrevStreetNumber = Convert.ToString(dr["PrevStreetNumber"]);
          PrevStreetName = Convert.ToString(dr["PrevStreetName"]);
          PrevStreetType = Convert.ToString(dr["PrevStreetType"]);
          PrevSuburb = (dr["PrevSuburb"] as Int32?) ?? null;
          PrevState = Convert.ToString(dr["PrevState"]);
          HavingDriverLicense = (Boolean)dr["HavingDriverLicense"];
          DriversLicenceNo = Convert.ToString(dr["DriversLicenceNo"]);
          LicenseStateOfIssue = Convert.ToString(dr["LicenseStateOfIssue"]);
          CurrentEmp1Name = Convert.ToString(dr["CurrentEmp1Name"]);
          CurrentEmp1AddressLine1 = Convert.ToString(dr["CurrentEmp1AddressLine1"]);
          CurrentEmp1AddressLine2 = Convert.ToString(dr["CurrentEmp1AddressLine2"]);
          CurrentEmp1lAs = Convert.ToString(dr["CurrentEmp1lAs"]);
          CurrentEmp1Industry = Convert.ToString(dr["CurrentEmp1Industry"]);
          CurrentEmp1PayPeriod = Convert.ToString(dr["CurrentEmp1PayPeriod"]);
          CurrentEmp1Since = (dr["CurrentEmp1Since"] as DateTime?) ?? null;
          CurrentEmp1Basis = Convert.ToString(dr["CurrentEmp1Basis"]);
          CurrentEmp1NetIncomeWeekly = (dr["CurrentEmp1NetIncomeWeekly"] as Decimal?) ?? null;
          CurrentEmp2Name = Convert.ToString(dr["CurrentEmp2Name"]);
          CurrentEmp2AddressLine1 = Convert.ToString(dr["CurrentEmp2AddressLine1"]);
          CurrentEmp2AddressLine2 = Convert.ToString(dr["CurrentEmp2AddressLine2"]);
          CurrentEmp2lAs = Convert.ToString(dr["CurrentEmp2lAs"]);
          CurrentEmp2Industry = Convert.ToString(dr["CurrentEmp2Industry"]);
          CurrentEmp2PayPeriod = Convert.ToString(dr["CurrentEmp2PayPeriod"]);
          CurrentEmp2Since = (dr["CurrentEmp2Since"] as DateTime?) ?? null;
          CurrentEmp2NetIncomeWeekly = (dr["CurrentEmp2NetIncomeWeekly"] as Decimal?) ?? null;
          CurrentEmp2Basis = Convert.ToString(dr["CurrentEmp2Basis"]);
          HavingSpouse = (Boolean)dr["HavingSpouse"];
          Title_2 = Convert.ToString(dr["Title_2"]);
          Surname_2 = Convert.ToString(dr["Surname_2"]);
          FirstName_2 = Convert.ToString(dr["FirstName_2"]);
          MiddleName_2 = Convert.ToString(dr["MiddleName_2"]);
          Nickname_2 = Convert.ToString(dr["Nickname_2"]);
          TelephoneHome_2 = Convert.ToString(dr["TelephoneHome_2"]);
          TelephoneWork_2 = Convert.ToString(dr["TelephoneWork_2"]);
          Mobile_2 = Convert.ToString(dr["Mobile_2"]);
          Fax_2 = Convert.ToString(dr["Fax_2"]);
          Email_2 = Convert.ToString(dr["Email_2"]);
          Email02_2 = Convert.ToString(dr["Email02_2"]);
          TelephoneHomePrefer_2 = (Boolean)dr["TelephoneHomePrefer_2"];
          TelephoneWorkPrefer_2 = (Boolean)dr["TelephoneWorkPrefer_2"];
          MobilePrefer_2 = (Boolean)dr["MobilePrefer_2"];
          SMSPrefer_2 = (Boolean)dr["SMSPrefer_2"];
          FaxPrefer_2 = (Boolean)dr["FaxPrefer_2"];
          EmailPrefer_2 = (Boolean)dr["EmailPrefer_2"];
          EmailSecondPrefer_2 = (Boolean)dr["EmailSecondPrefer_2"];
          PersonalRefName_2 = Convert.ToString(dr["PersonalRefName_2"]);
          PersonalRefAddress_2 = Convert.ToString(dr["PersonalRefAddress_2"]);
          PersonalRefRelationship_2 = Convert.ToString(dr["PersonalRefRelationship_2"]);
          PersonalRefPhone_2 = Convert.ToString(dr["PersonalRefPhone_2"]);
          Sex_2 = Convert.ToString(dr["Sex_2"]);
          DateOfBirth_2 = (dr["DateOfBirth_2"] as DateTime?) ?? null;
          HomeUnitNumber_2 = Convert.ToString(dr["HomeUnitNumber_2"]);
          HomeStreetNumber_2 = Convert.ToString(dr["HomeStreetNumber_2"]);
          HomeStreetName_2 = Convert.ToString(dr["HomeStreetName_2"]);
          HomeStreetType_2 = Convert.ToString(dr["HomeStreetType_2"]);
          HomeSuburb_2 = (dr["HomeSuburb_2"] as Int32?) ?? null;
          HomeState_2 = Convert.ToString(dr["HomeState_2"]);
          PostalUnitNumber_2 = Convert.ToString(dr["PostalUnitNumber_2"]);
          PostalStreetNumber_2 = Convert.ToString(dr["PostalStreetNumber_2"]);
          PostalStreetName_2 = Convert.ToString(dr["PostalStreetName_2"]);
          PostalStreetType_2 = Convert.ToString(dr["PostalStreetType_2"]);
          PostalSuburb_2 = (dr["PostalSuburb_2"] as Int32?) ?? null;
          PostalState_2 = Convert.ToString(dr["PostalState_2"]);
          PrevUnitNumber_2 = Convert.ToString(dr["PrevUnitNumber_2"]);
          PrevStreetNumber_2 = Convert.ToString(dr["PrevStreetNumber_2"]);
          PrevStreetName_2 = Convert.ToString(dr["PrevStreetName_2"]);
          PrevStreetType_2 = Convert.ToString(dr["PrevStreetType_2"]);
          PrevSuburb_2 = (dr["PrevSuburb_2"] as Int32?) ?? null;
          PrevState_2 = Convert.ToString(dr["PrevState_2"]);
          HavingDriverLicense_2 = (Boolean)dr["HavingDriverLicense_2"];
          DriversLicenceNo_2 = Convert.ToString(dr["DriversLicenceNo_2"]);
          LicenseStateOfIssue_2 = Convert.ToString(dr["LicenseStateOfIssue_2"]);
          CurrentEmp1Name_2 = Convert.ToString(dr["CurrentEmp1Name_2"]);
          CurrentEmp1AddressLine1_2 = Convert.ToString(dr["CurrentEmp1AddressLine1_2"]);
          CurrentEmp1AddressLine2_2 = Convert.ToString(dr["CurrentEmp1AddressLine2_2"]);
          CurrentEmp1lAs_2 = Convert.ToString(dr["CurrentEmp1lAs_2"]);
          CurrentEmp1Industry_2 = Convert.ToString(dr["CurrentEmp1Industry_2"]);
          CurrentEmp1PayPeriod_2 = Convert.ToString(dr["CurrentEmp1PayPeriod_2"]);
          CurrentEmp1Since_2 = (dr["CurrentEmp1Since_2"] as DateTime?) ?? null;
          CurrentEmp1Basis_2 = Convert.ToString(dr["CurrentEmp1Basis_2"]);
          CurrentEmp1NetIncomeWeekly_2 = (dr["CurrentEmp1NetIncomeWeekly_2"] as Decimal?) ?? null;
          CurrentEmp2Name_2 = Convert.ToString(dr["CurrentEmp2Name_2"]);
          CurrentEmp2AddressLine1_2 = Convert.ToString(dr["CurrentEmp2AddressLine1_2"]);
          CurrentEmp2AddressLine2_2 = Convert.ToString(dr["CurrentEmp2AddressLine2_2"]);
          CurrentEmp2lAs_2 = Convert.ToString(dr["CurrentEmp2lAs_2"]);
          CurrentEmp2Industry_2 = Convert.ToString(dr["CurrentEmp2Industry_2"]);
          CurrentEmp2PayPeriod_2 = Convert.ToString(dr["CurrentEmp2PayPeriod_2"]);
          CurrentEmp2Since_2 = (dr["CurrentEmp2Since_2"] as DateTime?) ?? null;
          CurrentEmp2NetIncomeWeekly_2 = (dr["CurrentEmp2NetIncomeWeekly_2"] as Decimal?) ?? null;
          CurrentEmp2Basis_2 = Convert.ToString(dr["CurrentEmp2Basis_2"]);
          NoOfDependents = (dr["NoOfDependents"] as Int32?) ?? null;
          AgesOfDependents = Convert.ToString(dr["AgesOfDependents"]);
          LegalActionWithCreditor = (Boolean)dr["LegalActionWithCreditor"];
          LegalActionCreditorName = Convert.ToString(dr["LegalActionCreditorName"]);
          FirstDifficultyPayingDebtYYYY = Convert.ToString(dr["FirstDifficultyPayingDebtYYYY"]);
          FirstDifficultyPayingDebtMMMM = Convert.ToString(dr["FirstDifficultyPayingDebtMMMM"]);
          FinDifficultyCauseID = (dr["FinDifficultyCauseID"] as Int32?) ?? null;
          FinDifficultyCauseOther = Convert.ToString(dr["FinDifficultyCauseOther"]);
          ESBChangeToIncome12Month = (Boolean)dr["ESBChangeToIncome12Month"];
          ESBChangeToIncome12MonthDesc = Convert.ToString(dr["ESBChangeToIncome12MonthDesc"]);
          PrevBankruptYear = Convert.ToString(dr["PrevBankruptYear"]);
          IncomeWageLast12M = (dr["IncomeWageLast12M"] as Decimal?) ?? null;
          IncomeGovBenefitLast12M = (dr["IncomeGovBenefitLast12M"] as Decimal?) ?? null;
          IncomeBusinessLast12M = (dr["IncomeBusinessLast12M"] as Decimal?) ?? null;
          IncomeTrustLast12M = (dr["IncomeTrustLast12M"] as Decimal?) ?? null;
          IncomeDividentLast12M = (dr["IncomeDividentLast12M"] as Decimal?) ?? null;
          IncomeOtherLast12M = (dr["IncomeOtherLast12M"] as Decimal?) ?? null;
          IncomeReceiveFromGov = Convert.ToString(dr["IncomeReceiveFromGov"]);
          IncomeReceiveFromBusiness = Convert.ToString(dr["IncomeReceiveFromBusiness"]);
          IncomeReceiveFromTrust = Convert.ToString(dr["IncomeReceiveFromTrust"]);
          IncomeReceiveFromDivident = Convert.ToString(dr["IncomeReceiveFromDivident"]);
          IncomeReceiveFromOther = Convert.ToString(dr["IncomeReceiveFromOther"]);
          PartnerNetWeeklyIncome = (dr["PartnerNetWeeklyIncome"] as Decimal?) ?? null;
          TotalIncomeAfterTax = (dr["TotalIncomeAfterTax"] as Decimal?) ?? null;
          TotalIncomeWithPartnerAfterTax = (dr["TotalIncomeWithPartnerAfterTax"] as Decimal?) ?? null;
          AgreedActualSurplus = (dr["AgreedActualSurplus"] as Decimal?) ?? null;
          Notes = Convert.ToString(dr["Notes"]);
          ApplicationManager = Convert.ToString(dr["ApplicationManager"]);
          Operator = Convert.ToString(dr["Operator"]);
          FileLocation = Convert.ToString(dr["FileLocation"]);
          SalesPackNumber = (dr["SalesPackNumber"] as Byte?) ?? null;
          ApplicationFee = (dr["ApplicationFee"] as Decimal?) ?? null;
          MonthlyManagementFee = (dr["MonthlyManagementFee"] as Decimal?) ?? null;
          //TotalBudgetToManage = (dr["TotalBudgetToManage"] as Decimal?) ?? null;

          PaymentFromClientStartDate = (dr["PaymentFromClientStartDate"] as DateTime?) ?? null;
          AgreedAmountToWithdrawFromClient = (dr["AgreedAmountToWithdrawFromClient"] as Decimal?) ?? null;
          WithdrawalFrequency = Convert.ToString(dr["WithdrawalFrequency"]);



        }
      }
    }

    public void Save()
    {
      //building the UPDATE string...
      StringBuilder sql = new StringBuilder();
      sql.Append("UPDATE BMApplicant SET ");
      sql.Append("SequenceID = @SequenceID, ");
      sql.Append("CallID = @CallID, ");
      sql.Append("CreateDate = @CreateDate, ");
      sql.Append("LastModifiedDate = @LastModifiedDate, ");
      sql.Append("BMStatusID = @BMStatusID, ");
      sql.Append("StatusStartDate = @StatusStartDate, ");
      sql.Append("StatusExpiryDate = @StatusExpiryDate, ");
      sql.Append("StatusEnteredBy = @StatusEnteredBy, ");
      sql.Append("StatusDate = @StatusDate, ");
      sql.Append("StatusDetails = @StatusDetails, ");
      sql.Append("CompanyID = @CompanyID, ");
      sql.Append("Referral = @Referral, ");
      sql.Append("Title = @Title, ");
      sql.Append("Surname = @Surname, ");
      sql.Append("FirstName = @FirstName, ");
      sql.Append("MiddleName = @MiddleName, ");
      sql.Append("Nickname = @Nickname, ");
      sql.Append("TelephoneHome = @TelephoneHome, ");
      sql.Append("TelephoneWork = @TelephoneWork, ");
      sql.Append("Mobile = @Mobile, ");
      sql.Append("Fax = @Fax, ");
      sql.Append("Email = @Email, ");
      sql.Append("Email02 = @Email02, ");
      sql.Append("TelephoneHomePrefer = @TelephoneHomePrefer, ");
      sql.Append("TelephoneWorkPrefer = @TelephoneWorkPrefer, ");
      sql.Append("MobilePrefer = @MobilePrefer, ");
      sql.Append("SMSPrefer = @SMSPrefer, ");
      sql.Append("FaxPrefer = @FaxPrefer, ");
      sql.Append("EmailPrefer = @EmailPrefer, ");
      sql.Append("EmailSecondPrefer = @EmailSecondPrefer, ");
      sql.Append("PersonalRefName = @PersonalRefName, ");
      sql.Append("PersonalRefAddress = @PersonalRefAddress, ");
      sql.Append("PersonalRefRelationship = @PersonalRefRelationship, ");
      sql.Append("PersonalRefPhone = @PersonalRefPhone, ");
      sql.Append("Sex = @Sex, ");
      sql.Append("DateOfBirth = @DateOfBirth, ");
      sql.Append("HomeUnitNumber = @HomeUnitNumber, ");
      sql.Append("HomeStreetNumber = @HomeStreetNumber, ");
      sql.Append("HomeStreetName = @HomeStreetName, ");
      sql.Append("HomeStreetType = @HomeStreetType, ");
      sql.Append("HomeSuburb = @HomeSuburb, ");
      sql.Append("HomeState = @HomeState, ");
      sql.Append("PostalUnitNumber = @PostalUnitNumber, ");
      sql.Append("PostalStreetNumber = @PostalStreetNumber, ");
      sql.Append("PostalStreetName = @PostalStreetName, ");
      sql.Append("PostalStreetType = @PostalStreetType, ");
      sql.Append("PostalSuburb = @PostalSuburb, ");
      sql.Append("PostalState = @PostalState, ");
      sql.Append("PrevUnitNumber = @PrevUnitNumber, ");
      sql.Append("PrevStreetNumber = @PrevStreetNumber, ");
      sql.Append("PrevStreetName = @PrevStreetName, ");
      sql.Append("PrevStreetType = @PrevStreetType, ");
      sql.Append("PrevSuburb = @PrevSuburb, ");
      sql.Append("PrevState = @PrevState, ");
      sql.Append("HavingDriverLicense = @HavingDriverLicense, ");
      sql.Append("DriversLicenceNo = @DriversLicenceNo, ");
      sql.Append("LicenseStateOfIssue = @LicenseStateOfIssue, ");
      sql.Append("CurrentEmp1Name = @CurrentEmp1Name, ");
      sql.Append("CurrentEmp1AddressLine1 = @CurrentEmp1AddressLine1, ");
      sql.Append("CurrentEmp1AddressLine2 = @CurrentEmp1AddressLine2, ");
      sql.Append("CurrentEmp1lAs = @CurrentEmp1lAs, ");
      sql.Append("CurrentEmp1Industry = @CurrentEmp1Industry, ");
      sql.Append("CurrentEmp1PayPeriod = @CurrentEmp1PayPeriod, ");
      sql.Append("CurrentEmp1Since = @CurrentEmp1Since, ");
      sql.Append("CurrentEmp1Basis = @CurrentEmp1Basis, ");
      sql.Append("CurrentEmp1NetIncomeWeekly = @CurrentEmp1NetIncomeWeekly, ");
      sql.Append("CurrentEmp2Name = @CurrentEmp2Name, ");
      sql.Append("CurrentEmp2AddressLine1 = @CurrentEmp2AddressLine1, ");
      sql.Append("CurrentEmp2AddressLine2 = @CurrentEmp2AddressLine2, ");
      sql.Append("CurrentEmp2lAs = @CurrentEmp2lAs, ");
      sql.Append("CurrentEmp2Industry = @CurrentEmp2Industry, ");
      sql.Append("CurrentEmp2PayPeriod = @CurrentEmp2PayPeriod, ");
      sql.Append("CurrentEmp2Since = @CurrentEmp2Since, ");
      sql.Append("CurrentEmp2NetIncomeWeekly = @CurrentEmp2NetIncomeWeekly, ");
      sql.Append("CurrentEmp2Basis = @CurrentEmp2Basis, ");
      sql.Append("HavingSpouse = @HavingSpouse, ");
      sql.Append("Title_2 = @Title_2, ");
      sql.Append("Surname_2 = @Surname_2, ");
      sql.Append("FirstName_2 = @FirstName_2, ");
      sql.Append("MiddleName_2 = @MiddleName_2, ");
      sql.Append("Nickname_2 = @Nickname_2, ");
      sql.Append("TelephoneHome_2 = @TelephoneHome_2, ");
      sql.Append("TelephoneWork_2 = @TelephoneWork_2, ");
      sql.Append("Mobile_2 = @Mobile_2, ");
      sql.Append("Fax_2 = @Fax_2, ");
      sql.Append("Email_2 = @Email_2, ");
      sql.Append("Email02_2 = @Email02_2, ");
      sql.Append("TelephoneHomePrefer_2 = @TelephoneHomePrefer_2, ");
      sql.Append("TelephoneWorkPrefer_2 = @TelephoneWorkPrefer_2, ");
      sql.Append("MobilePrefer_2 = @MobilePrefer_2, ");
      sql.Append("SMSPrefer_2 = @SMSPrefer_2, ");
      sql.Append("FaxPrefer_2 = @FaxPrefer_2, ");
      sql.Append("EmailPrefer_2 = @EmailPrefer_2, ");
      sql.Append("EmailSecondPrefer_2 = @EmailSecondPrefer_2, ");
      sql.Append("PersonalRefName_2 = @PersonalRefName_2, ");
      sql.Append("PersonalRefAddress_2 = @PersonalRefAddress_2, ");
      sql.Append("PersonalRefRelationship_2 = @PersonalRefRelationship_2, ");
      sql.Append("PersonalRefPhone_2 = @PersonalRefPhone_2, ");
      sql.Append("Sex_2 = @Sex_2, ");
      sql.Append("DateOfBirth_2 = @DateOfBirth_2, ");
      sql.Append("HomeUnitNumber_2 = @HomeUnitNumber_2, ");
      sql.Append("HomeStreetNumber_2 = @HomeStreetNumber_2, ");
      sql.Append("HomeStreetName_2 = @HomeStreetName_2, ");
      sql.Append("HomeStreetType_2 = @HomeStreetType_2, ");
      sql.Append("HomeSuburb_2 = @HomeSuburb_2, ");
      sql.Append("HomeState_2 = @HomeState_2, ");
      sql.Append("PostalUnitNumber_2 = @PostalUnitNumber_2, ");
      sql.Append("PostalStreetNumber_2 = @PostalStreetNumber_2, ");
      sql.Append("PostalStreetName_2 = @PostalStreetName_2, ");
      sql.Append("PostalStreetType_2 = @PostalStreetType_2, ");
      sql.Append("PostalSuburb_2 = @PostalSuburb_2, ");
      sql.Append("PostalState_2 = @PostalState_2, ");
      sql.Append("PrevUnitNumber_2 = @PrevUnitNumber_2, ");
      sql.Append("PrevStreetNumber_2 = @PrevStreetNumber_2, ");
      sql.Append("PrevStreetName_2 = @PrevStreetName_2, ");
      sql.Append("PrevStreetType_2 = @PrevStreetType_2, ");
      sql.Append("PrevSuburb_2 = @PrevSuburb_2, ");
      sql.Append("PrevState_2 = @PrevState_2, ");
      sql.Append("HavingDriverLicense_2 = @HavingDriverLicense_2, ");
      sql.Append("DriversLicenceNo_2 = @DriversLicenceNo_2, ");
      sql.Append("LicenseStateOfIssue_2 = @LicenseStateOfIssue_2, ");
      sql.Append("CurrentEmp1Name_2 = @CurrentEmp1Name_2, ");
      sql.Append("CurrentEmp1AddressLine1_2 = @CurrentEmp1AddressLine1_2, ");
      sql.Append("CurrentEmp1AddressLine2_2 = @CurrentEmp1AddressLine2_2, ");
      sql.Append("CurrentEmp1lAs_2 = @CurrentEmp1lAs_2, ");
      sql.Append("CurrentEmp1Industry_2 = @CurrentEmp1Industry_2, ");
      sql.Append("CurrentEmp1PayPeriod_2 = @CurrentEmp1PayPeriod_2, ");
      sql.Append("CurrentEmp1Since_2 = @CurrentEmp1Since_2, ");
      sql.Append("CurrentEmp1Basis_2 = @CurrentEmp1Basis_2, ");
      sql.Append("CurrentEmp1NetIncomeWeekly_2 = @CurrentEmp1NetIncomeWeekly_2, ");
      sql.Append("CurrentEmp2Name_2 = @CurrentEmp2Name_2, ");
      sql.Append("CurrentEmp2AddressLine1_2 = @CurrentEmp2AddressLine1_2, ");
      sql.Append("CurrentEmp2AddressLine2_2 = @CurrentEmp2AddressLine2_2, ");
      sql.Append("CurrentEmp2lAs_2 = @CurrentEmp2lAs_2, ");
      sql.Append("CurrentEmp2Industry_2 = @CurrentEmp2Industry_2, ");
      sql.Append("CurrentEmp2PayPeriod_2 = @CurrentEmp2PayPeriod_2, ");
      sql.Append("CurrentEmp2Since_2 = @CurrentEmp2Since_2, ");
      sql.Append("CurrentEmp2NetIncomeWeekly_2 = @CurrentEmp2NetIncomeWeekly_2, ");
      sql.Append("CurrentEmp2Basis_2 = @CurrentEmp2Basis_2, ");
      sql.Append("NoOfDependents = @NoOfDependents, ");
      sql.Append("AgesOfDependents = @AgesOfDependents, ");
      sql.Append("LegalActionWithCreditor = @LegalActionWithCreditor, ");
      sql.Append("LegalActionCreditorName = @LegalActionCreditorName, ");
      sql.Append("FirstDifficultyPayingDebtYYYY = @FirstDifficultyPayingDebtYYYY, ");
      sql.Append("FirstDifficultyPayingDebtMMMM = @FirstDifficultyPayingDebtMMMM, ");
      sql.Append("FinDifficultyCauseID = @FinDifficultyCauseID, ");
      sql.Append("FinDifficultyCauseOther = @FinDifficultyCauseOther, ");
      sql.Append("ESBChangeToIncome12Month = @ESBChangeToIncome12Month, ");
      sql.Append("ESBChangeToIncome12MonthDesc = @ESBChangeToIncome12MonthDesc, ");
      sql.Append("PrevBankruptYear = @PrevBankruptYear, ");
      sql.Append("IncomeWageLast12M = @IncomeWageLast12M, ");
      sql.Append("IncomeGovBenefitLast12M = @IncomeGovBenefitLast12M, ");
      sql.Append("IncomeBusinessLast12M = @IncomeBusinessLast12M, ");
      sql.Append("IncomeTrustLast12M = @IncomeTrustLast12M, ");
      sql.Append("IncomeDividentLast12M = @IncomeDividentLast12M, ");
      sql.Append("IncomeOtherLast12M = @IncomeOtherLast12M, ");
      sql.Append("IncomeReceiveFromGov = @IncomeReceiveFromGov, ");
      sql.Append("IncomeReceiveFromBusiness = @IncomeReceiveFromBusiness, ");
      sql.Append("IncomeReceiveFromTrust = @IncomeReceiveFromTrust, ");
      sql.Append("IncomeReceiveFromDivident = @IncomeReceiveFromDivident, ");
      sql.Append("IncomeReceiveFromOther = @IncomeReceiveFromOther, ");
      sql.Append("PartnerNetWeeklyIncome = @PartnerNetWeeklyIncome, ");
      sql.Append("TotalIncomeAfterTax = @TotalIncomeAfterTax, ");
      sql.Append("TotalIncomeWithPartnerAfterTax = @TotalIncomeWithPartnerAfterTax, ");
      sql.Append("AgreedActualSurplus = @AgreedActualSurplus, ");
      sql.Append("Notes = @Notes, ");
      sql.Append("ApplicationManager = @ApplicationManager, ");
      sql.Append("Operator = @Operator, ");
      sql.Append("FileLocation = @FileLocation, ");
      sql.Append("SalesPackNumber = @SalesPackNumber, ");
      sql.Append("ApplicationFee = @ApplicationFee, ");
      sql.Append("MonthlyManagementFee = @MonthlyManagementFee, ");
      sql.Append("TotalBudgetToManage = @TotalBudgetToManage, ");

      sql.Append("WHERE ApplicantID = @ApplicantID");

      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand(sql.ToString(), conn);

        cmd.Parameters.Add(new SqlParameter("ApplicantID", ((object)ApplicantID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("SequenceID", ((object)SequenceID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CallID", ((object)CallID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CreateDate", ((object)CreateDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("LastModifiedDate", ((object)LastModifiedDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("BMStatusID", ((object)BMStatusID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusStartDate", ((object)StatusStartDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusExpiryDate", ((object)StatusExpiryDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusEnteredBy", ((object)StatusEnteredBy ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusDate", ((object)StatusDate ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("StatusDetails", ((object)StatusDetails ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CompanyID", ((object)CompanyID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Referral", ((object)Referral ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Title", ((object)Title ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Surname", ((object)Surname ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FirstName", ((object)FirstName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("MiddleName", ((object)MiddleName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Nickname", ((object)Nickname ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneHome", ((object)TelephoneHome ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneWork", ((object)TelephoneWork ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Mobile", ((object)Mobile ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Fax", ((object)Fax ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Email", ((object)Email ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Email02", ((object)Email02 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneHomePrefer", ((object)TelephoneHomePrefer ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneWorkPrefer", ((object)TelephoneWorkPrefer ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("MobilePrefer", ((object)MobilePrefer ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("SMSPrefer", ((object)SMSPrefer ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FaxPrefer", ((object)FaxPrefer ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("EmailPrefer", ((object)EmailPrefer ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("EmailSecondPrefer", ((object)EmailSecondPrefer ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefName", ((object)PersonalRefName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefAddress", ((object)PersonalRefAddress ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefRelationship", ((object)PersonalRefRelationship ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefPhone", ((object)PersonalRefPhone ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Sex", ((object)Sex ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DateOfBirth", ((object)DateOfBirth ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeUnitNumber", ((object)HomeUnitNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetNumber", ((object)HomeStreetNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetName", ((object)HomeStreetName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetType", ((object)HomeStreetType ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeSuburb", ((object)HomeSuburb ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeState", ((object)HomeState ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalUnitNumber", ((object)PostalUnitNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetNumber", ((object)PostalStreetNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetName", ((object)PostalStreetName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetType", ((object)PostalStreetType ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalSuburb", ((object)PostalSuburb ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalState", ((object)PostalState ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevUnitNumber", ((object)PrevUnitNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetNumber", ((object)PrevStreetNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetName", ((object)PrevStreetName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetType", ((object)PrevStreetType ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevSuburb", ((object)PrevSuburb ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevState", ((object)PrevState ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HavingDriverLicense", ((object)HavingDriverLicense ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DriversLicenceNo", ((object)DriversLicenceNo ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("LicenseStateOfIssue", ((object)LicenseStateOfIssue ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1Name", ((object)CurrentEmp1Name ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1AddressLine1", ((object)CurrentEmp1AddressLine1 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1AddressLine2", ((object)CurrentEmp1AddressLine2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1lAs", ((object)CurrentEmp1lAs ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1Industry", ((object)CurrentEmp1Industry ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1PayPeriod", ((object)CurrentEmp1PayPeriod ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1Since", ((object)CurrentEmp1Since ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1Basis", ((object)CurrentEmp1Basis ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1NetIncomeWeekly", ((object)CurrentEmp1NetIncomeWeekly ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2Name", ((object)CurrentEmp2Name ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2AddressLine1", ((object)CurrentEmp2AddressLine1 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2AddressLine2", ((object)CurrentEmp2AddressLine2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2lAs", ((object)CurrentEmp2lAs ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2Industry", ((object)CurrentEmp2Industry ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2PayPeriod", ((object)CurrentEmp2PayPeriod ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2Since", ((object)CurrentEmp2Since ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2NetIncomeWeekly", ((object)CurrentEmp2NetIncomeWeekly ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2Basis", ((object)CurrentEmp2Basis ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HavingSpouse", ((object)HavingSpouse ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Title_2", ((object)Title_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Surname_2", ((object)Surname_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FirstName_2", ((object)FirstName_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("MiddleName_2", ((object)MiddleName_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Nickname_2", ((object)Nickname_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneHome_2", ((object)TelephoneHome_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneWork_2", ((object)TelephoneWork_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Mobile_2", ((object)Mobile_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Fax_2", ((object)Fax_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Email_2", ((object)Email_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Email02_2", ((object)Email02_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneHomePrefer_2", ((object)TelephoneHomePrefer_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TelephoneWorkPrefer_2", ((object)TelephoneWorkPrefer_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("MobilePrefer_2", ((object)MobilePrefer_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("SMSPrefer_2", ((object)SMSPrefer_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FaxPrefer_2", ((object)FaxPrefer_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("EmailPrefer_2", ((object)EmailPrefer_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("EmailSecondPrefer_2", ((object)EmailSecondPrefer_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefName_2", ((object)PersonalRefName_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefAddress_2", ((object)PersonalRefAddress_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefRelationship_2", ((object)PersonalRefRelationship_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PersonalRefPhone_2", ((object)PersonalRefPhone_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Sex_2", ((object)Sex_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DateOfBirth_2", ((object)DateOfBirth_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeUnitNumber_2", ((object)HomeUnitNumber_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetNumber_2", ((object)HomeStreetNumber_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetName_2", ((object)HomeStreetName_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeStreetType_2", ((object)HomeStreetType_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeSuburb_2", ((object)HomeSuburb_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HomeState_2", ((object)HomeState_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalUnitNumber_2", ((object)PostalUnitNumber_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetNumber_2", ((object)PostalStreetNumber_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetName_2", ((object)PostalStreetName_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalStreetType_2", ((object)PostalStreetType_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalSuburb_2", ((object)PostalSuburb_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PostalState_2", ((object)PostalState_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevUnitNumber_2", ((object)PrevUnitNumber_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetNumber_2", ((object)PrevStreetNumber_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetName_2", ((object)PrevStreetName_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevStreetType_2", ((object)PrevStreetType_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevSuburb_2", ((object)PrevSuburb_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevState_2", ((object)PrevState_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("HavingDriverLicense_2", ((object)HavingDriverLicense_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("DriversLicenceNo_2", ((object)DriversLicenceNo_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("LicenseStateOfIssue_2", ((object)LicenseStateOfIssue_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1Name_2", ((object)CurrentEmp1Name_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1AddressLine1_2", ((object)CurrentEmp1AddressLine1_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1AddressLine2_2", ((object)CurrentEmp1AddressLine2_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1lAs_2", ((object)CurrentEmp1lAs_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1Industry_2", ((object)CurrentEmp1Industry_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1PayPeriod_2", ((object)CurrentEmp1PayPeriod_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1Since_2", ((object)CurrentEmp1Since_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1Basis_2", ((object)CurrentEmp1Basis_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp1NetIncomeWeekly_2", ((object)CurrentEmp1NetIncomeWeekly_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2Name_2", ((object)CurrentEmp2Name_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2AddressLine1_2", ((object)CurrentEmp2AddressLine1_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2AddressLine2_2", ((object)CurrentEmp2AddressLine2_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2lAs_2", ((object)CurrentEmp2lAs_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2Industry_2", ((object)CurrentEmp2Industry_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2PayPeriod_2", ((object)CurrentEmp2PayPeriod_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2Since_2", ((object)CurrentEmp2Since_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2NetIncomeWeekly_2", ((object)CurrentEmp2NetIncomeWeekly_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("CurrentEmp2Basis_2", ((object)CurrentEmp2Basis_2 ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("NoOfDependents", ((object)NoOfDependents ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgesOfDependents", ((object)AgesOfDependents ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("LegalActionWithCreditor", ((object)LegalActionWithCreditor ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("LegalActionCreditorName", ((object)LegalActionCreditorName ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FirstDifficultyPayingDebtYYYY", ((object)FirstDifficultyPayingDebtYYYY ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FirstDifficultyPayingDebtMMMM", ((object)FirstDifficultyPayingDebtMMMM ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FinDifficultyCauseID", ((object)FinDifficultyCauseID ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FinDifficultyCauseOther", ((object)FinDifficultyCauseOther ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ESBChangeToIncome12Month", ((object)ESBChangeToIncome12Month ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ESBChangeToIncome12MonthDesc", ((object)ESBChangeToIncome12MonthDesc ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PrevBankruptYear", ((object)PrevBankruptYear ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeWageLast12M", ((object)IncomeWageLast12M ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeGovBenefitLast12M", ((object)IncomeGovBenefitLast12M ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeBusinessLast12M", ((object)IncomeBusinessLast12M ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeTrustLast12M", ((object)IncomeTrustLast12M ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeDividentLast12M", ((object)IncomeDividentLast12M ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeOtherLast12M", ((object)IncomeOtherLast12M ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeReceiveFromGov", ((object)IncomeReceiveFromGov ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeReceiveFromBusiness", ((object)IncomeReceiveFromBusiness ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeReceiveFromTrust", ((object)IncomeReceiveFromTrust ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeReceiveFromDivident", ((object)IncomeReceiveFromDivident ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("IncomeReceiveFromOther", ((object)IncomeReceiveFromOther ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("PartnerNetWeeklyIncome", ((object)PartnerNetWeeklyIncome ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TotalIncomeAfterTax", ((object)TotalIncomeAfterTax ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TotalIncomeWithPartnerAfterTax", ((object)TotalIncomeWithPartnerAfterTax ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("AgreedActualSurplus", ((object)AgreedActualSurplus ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Notes", ((object)Notes ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationManager", ((object)ApplicationManager ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("Operator", ((object)Operator ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("FileLocation", ((object)FileLocation ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("SalesPackNumber", ((object)SalesPackNumber ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("ApplicationFee", ((object)ApplicationFee ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("MonthlyManagementFee", ((object)MonthlyManagementFee ?? DBNull.Value)));
        cmd.Parameters.Add(new SqlParameter("TotalBudgetToManage", ((object)TotalBudgetToManage ?? DBNull.Value)));

        conn.Open();
        cmd.ExecuteNonQuery();
      }
    }
    public void LoadCompany()
    {
      Company = new MMCompany(Convert.ToInt32(CompanyID));
    }
    //Debt Table Rendering
    public string html_debt_table
    {
      get
      {
        if (string.IsNullOrEmpty(_html_debt_table))
        {
          //populating _html_debt_table items ready for email display
          decimal TotalMonthly = 0;
          StringBuilder s = new StringBuilder();
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            SqlCommand cmd = new SqlCommand("SELECT * FROM BMAssetLiability WHERE ToBeIncluded = 1 AND ApplicantID = " + ApplicantID.ToString() + " ORDER BY TotalAmountOwing DESC", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            s.Append("<table width=100% cellpadding=8><tr><th align='left' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Creditor</th><th align='left' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Account No</th><th align='right' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Balance</th><th align='right' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Percentage</th><th align='right' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Monthly</th></tr>");
            
            while (dr.Read())
            {
              s.Append("<tr><td valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + dr["CreditorName"] + "</td><td valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + dr["AccountLoanNumber"] + "</td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + String.Format("{0:C}", dr["TotalAmountOwing"]) + "</td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + Math.Round(((decimal)dr["Percentage"] * 100), 2) + "%</td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + String.Format("{0:C}", dr["MonthlyPaymentToCreditorFromSurplus_ACTUAL"]) + "</td></tr>" + System.Environment.NewLine);
              TotalMonthly  += (decimal)(dr["MonthlyPaymentToCreditorFromSurplus_ACTUAL"]?? 0);

            }
          }
          if (s.Length <= 0) { _html_debt_table = "No debt items."; }
          else
          {
            //s.Append("<tr><td></td><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;font-weight:bold'>Totals:</td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;background-color:#DDD;font-weight:bold'>" + string.Format("{0:C}", TotalBudgetToManage) + "</td><td></td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;background-color:#DDD;font-weight:bold'>" + string.Format("{0:C}", (AgreedActualSurplus - MonthlyManagementFee)) + "</td></tr></table>");
            s.Append("<tr><td></td><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;font-weight:bold'>Totals:</td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;background-color:#DDD;font-weight:bold'>" + string.Format("{0:C}", TotalBudgetToManage) + "</td><td></td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;background-color:#DDD;font-weight:bold'>" + string.Format("{0:C}", TotalMonthly) + "</td></tr></table>");
            _html_debt_table = s.ToString();
          }
        }
        return _html_debt_table;
      }
    }
    public void LogContact(string ContactMadeBy, string Caption, string Notes, Nullable<Int32> EventDefID)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        SqlCommand cmd = new SqlCommand("INSERT INTO BMContactLog (ApplicantID, ContactMadeBy, ContactDate, Caption, Notes, BMEventDefID) VALUES (@1,@2,@3,@4,@5,@6)");
        cmd.Parameters.Add(new SqlParameter("@1", ApplicantID.ToString()));
        cmd.Parameters.Add(new SqlParameter("@2", ContactMadeBy));
        cmd.Parameters.Add(new SqlParameter("@3", DateTime.Now));
        cmd.Parameters.Add(new SqlParameter("@4", Caption));
        cmd.Parameters.Add(new SqlParameter("@5", Notes));
        cmd.Parameters.Add(new SqlParameter("@6", (object)EventDefID ?? DBNull.Value));

        conn.Open();
        cmd.Connection = conn;
        cmd.ExecuteNonQuery();
      }

    }
    public string html_debt_table_mini
    {
      get
      {
        if (string.IsNullOrEmpty(_html_debt_table_mini))
        {
          //populating _html_debt_table items ready for email display
          StringBuilder s = new StringBuilder();
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            SqlCommand cmd = new SqlCommand("SELECT * FROM BMAssetLiability WHERE ToBeIncluded = 1 AND ApplicantID = " + ApplicantID.ToString() + " ORDER BY TotalAmountOwing DESC", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            s.Append("<table width=100% cellpadding=8><tr><th align='left' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Creditor</th><th align='left' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Account No</th><th align='right' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'> Approximate Balance</th></tr>");
            while (dr.Read())
            {
              s.Append("<tr><td valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + dr["CreditorName"] + "</td><td valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + dr["AccountLoanNumber"] + "</td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + String.Format("{0:C}", dr["TotalAmountOwing"]) + "</td></tr>" + System.Environment.NewLine);
            }
          }
          if (s.Length <= 0) { _html_debt_table_mini = "No debt items."; }
          else
          {
            s.Append("<tr><td></td><td style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;font-weight:bold'>Totals:</td><td valign='top' align='right' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;font-weight:bold'>" + string.Format("{0:C}", TotalBudgetToManage) + "</td></tr></table>");
            _html_debt_table_mini = s.ToString();
          }
        }
        return _html_debt_table_mini;
      }
    }
    public string html_debt_table_micro
    {
      get
      {
        if (string.IsNullOrEmpty(_html_debt_table_micro))
        {
          //populating _html_debt_table items ready for email display
          StringBuilder s = new StringBuilder();
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            SqlCommand cmd = new SqlCommand("SELECT * FROM BMAssetLiability WHERE ToBeIncluded = 1 AND ApplicantID = " + ApplicantID.ToString() + " ORDER BY TotalAmountOwing DESC", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            s.Append("<table width=100% cellpadding=8><tr><th align='left' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Creditor</th><th align='left' style='background-color:#DDD; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>Account No</th></tr>");
            while (dr.Read())
            {
              s.Append("<tr><td valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + dr["CreditorName"] + "</td><td valign='top' style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;'>" + dr["AccountLoanNumber"] + "</td></tr>" + System.Environment.NewLine);
            }
          }
          if (s.Length <= 0) { _html_debt_table_micro = "No debt items."; }
          else
          {
            s.Append("</table>");
            _html_debt_table_micro = s.ToString();
          }
        }
        return _html_debt_table_micro;
      }
    }
    //ChecklistRendering
    public string CheckList_Outstanding
    {
      get
      {
        if (string.IsNullOrEmpty(_CheckList_Outstanding))
        {
          //populating _CheckList_Outstanding items ready for email display
          StringBuilder s = new StringBuilder();
          using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
          {
            SqlCommand cmd = new SqlCommand("SELECT * FROM CheckListLApplication WHERE BMApplicantID = " + ApplicantID.ToString() + " AND [Received] = 0 ORDER BY LabelOrder", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
              s.Append("<li style='font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #4d4e4f'>" + dr["Label"]);
              if (!(Convert.ToString(dr["ItemNote"]) == ""))
              {
                s.Append(" [" + dr["ItemNote"] + "]</li>" + System.Environment.NewLine);
              }
              else
              {
                s.Append("</li>" + System.Environment.NewLine);
              }

            }
          }
          if (s.Length <= 0) { _CheckList_Outstanding = "No outstanding items."; }
          else
          {
            _CheckList_Outstanding = "<ul>" + System.Environment.NewLine + s.ToString() + "</ul>";
          }
        }
        return _CheckList_Outstanding;
      }
    }
  }


}