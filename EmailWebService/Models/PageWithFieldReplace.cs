﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;

namespace EmailWebService.Models
{
	public class PageWithFieldReplace:Page
	{
		protected override void Render(HtmlTextWriter writer)
		{
			if (Session["Application"] != null)
			{
				MMApplication a = (MMApplication)Session["Application"];

				StringWriter output = new StringWriter();
				base.Render(new HtmlTextWriter(output));
				//This is the rendered HTML of your page. Feel free to manipulate it.

				string outputAsString = output.ToString().Replace("[CompanyName]", a.Company.Company);

				writer.Write(outputAsString);
			}
			else
			{ Response.Write("Session expired."); }
		}
	}
}