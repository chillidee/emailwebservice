﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace EmailWebService
{
	static class Utils
	{
		public static Nullable<int> EmptyToNull(string s)
		{
			if (String.IsNullOrEmpty(s))
			{
				return null;
			}
			else
			{
				return Convert.ToInt32(s);
			}
		}
		public static int EmptyToZero(string s)
		{
			if (String.IsNullOrEmpty(s)) return 0; 
			else return Convert.ToInt32(s);
		}
    public static string GetKeyword(string strU)
    {
      Regex regKeyword = new Regex("(p|q)=(?<keyword>.*?)&", RegexOptions.IgnoreCase | RegexOptions.Multiline);
      Regex regSnippet = new Regex("forgetcode.com.*%2F(?<id>\\d+)%2F", RegexOptions.IgnoreCase | RegexOptions.Multiline);
      Match match = regKeyword.Match(strU);
      string keyword = match.Groups["keyword"].ToString();
      // Get the decoded URL
      string result = HttpUtility.UrlDecode(keyword);
      // Get the HTML representation
      result = HttpUtility.HtmlEncode(result);
      return result;
    }
    public static string CleanFileName(string fileName)
    {
      return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
    } 
	}
	public struct DateTimeSpan
	{
		private readonly int years;
		private readonly int months;
		private readonly int days;
		private readonly int hours;
		private readonly int minutes;
		private readonly int seconds;
		private readonly int milliseconds;

		public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
		{
			this.years = years;
			this.months = months;
			this.days = days;
			this.hours = hours;
			this.minutes = minutes;
			this.seconds = seconds;
			this.milliseconds = milliseconds;
		}

		public int Years { get { return years; } }
		public int Months { get { return months; } }
		public int Days { get { return days; } }
		public int Hours { get { return hours; } }
		public int Minutes { get { return minutes; } }
		public int Seconds { get { return seconds; } }
		public int Milliseconds { get { return milliseconds; } }

		enum Phase { Years, Months, Days, Done }

		public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
		{

			if (date2 < date1)
			{
				var sub = date1;
				date1 = date2;
				date2 = sub;
			}

			DateTime current = date2;
			int years = 0;
			int months = 0;
			int days = 0;

			Phase phase = Phase.Years;
			DateTimeSpan span = new DateTimeSpan();

			while (phase != Phase.Done)
			{
				switch (phase)
				{
					case Phase.Years:
						if (current.Year == 1 || current.AddYears(-1) < date1)
						{
							phase = Phase.Months;
						}
						else
						{
							current = current.AddYears(-1);
							years++;
						}
						break;
					case Phase.Months:
						if (current.AddMonths(-1) < date1)
						{
							phase = Phase.Days;
						}
						else
						{
							current = current.AddMonths(-1);
							months++;
						}
						break;
					case Phase.Days:
						if (current.AddDays(-1) < date1)
						{
							var timespan = current - date1;
							span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
							phase = Phase.Done;
						}
						else
						{
							current = current.AddDays(-1);
							days++;
						}
						break;
				}
			}
			return span;
		}

 
	}
}