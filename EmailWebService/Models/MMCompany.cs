﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EmailWebService.Models
{
	public class MMCompany
	{
		public int CompanyID { get; set; }
		public string Company { get; set; }
		public string FullCompanyName { get; set; }
		public string Acronym { get; set; }
		public string ABN { get; set; }
		public string CreditLicNo { get; set; }
		public Nullable<byte> SortOrder { get; set; }
		public string Phone { get; set; }
		public string Fax { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string Suburb { get; set; }
		public string PostCode { get; set; }
		public string State { get; set; }
		public string Email { get; set; }
		public string LoanWriter { get; set; }
		public string AccountBank { get; set; }
		public string AccountName { get; set; }
		public string AccountBSB { get; set; }
		public string AccountNo { get; set; }
		public string ACN { get; set; }
		public Nullable<bool> IsLead { get; set; }
		public string WebApplicationLogoImageFilename { get; set; }
		public string WebsiteURL { get; set; }
		public string FacebookURL { get; set; }
		public string TwitterURL { get; set; }
		public string YouTubeURL { get; set; }
		public string AboutUsURL { get; set; }
		public string ContactUsURL { get; set; }
    public string LinkedInURL { get; set; }
    public string GooglePlusURL { get; set; }

		public MMCompany(Int32 ID)
		{
			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM Company WHERE CompanyID = " + ID.ToString(), conn);
				conn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read())
				{
					CompanyID = (dr["CompanyID"] as Int32?) ?? 0;
					Company = Convert.ToString(dr["Company"]);
					FullCompanyName = Convert.ToString(dr["FullCompanyName"]);
					Acronym = Convert.ToString(dr["Acronym"]);
					ABN = Convert.ToString(dr["ABN"]);
					CreditLicNo = Convert.ToString(dr["CreditLicNo"]);
					SortOrder = (dr["SortOrder"] as Byte?) ?? null;
					Phone = Convert.ToString(dr["Phone"]);
					Fax = Convert.ToString(dr["Fax"]);
					AddressLine1 = Convert.ToString(dr["AddressLine1"]);
					AddressLine2 = Convert.ToString(dr["AddressLine2"]);
					Suburb = Convert.ToString(dr["Suburb"]);
					PostCode = Convert.ToString(dr["PostCode"]);
					State = Convert.ToString(dr["State"]);
					Email = Convert.ToString(dr["Email"]);
					LoanWriter = Convert.ToString(dr["LoanWriter"]);
					AccountBank = Convert.ToString(dr["AccountBank"]);
					AccountName = Convert.ToString(dr["AccountName"]);
					AccountBSB = Convert.ToString(dr["AccountBSB"]);
					AccountNo = Convert.ToString(dr["AccountNo"]);
					ACN = Convert.ToString(dr["ACN"]);
					IsLead = (dr["IsLead"] as Boolean?) ?? null;
					WebApplicationLogoImageFilename = Convert.ToString(dr["WebApplicationLogoImageFilename"]);
					WebsiteURL = Convert.ToString(dr["WebsiteURL"]);
					FacebookURL = Convert.ToString(dr["FacebookURL"]);
					TwitterURL = Convert.ToString(dr["TwitterURL"]);
					YouTubeURL = Convert.ToString(dr["YouTubeURL"]);
					AboutUsURL = Convert.ToString(dr["AboutUsURL"]);
					ContactUsURL = Convert.ToString(dr["ContactUsURL"]);
          LinkedInURL = Convert.ToString(dr["LinkedInURL"]);
          GooglePlusURL = Convert.ToString(dr["GooglePlusURL"]);

				}
			}
		}
	}

}