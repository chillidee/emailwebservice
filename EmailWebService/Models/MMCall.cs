﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EmailWebService.Models
{
  public class MMCall
  {
    public int CallID { get; set; }
    public string GUID { get; set; }
    public string Title { get; set; }
    public string Surname { get; set; }
    public string FirstName { get; set; }
    public string Nickname { get; set; }
    public string TelephoneHome { get; set; }
    public string TelephoneWork { get; set; }
    public string Mobile { get; set; }
    public string Fax { get; set; }
    public string Email { get; set; }
    public string EmailSecond { get; set; }
    public System.DateTime CallDate { get; set; }
    public string Operator { get; set; }
    public string OperatorPhoneNumber { get; set; }
    public string Notes { get; set; }
    public string HomeAddressLine1 { get; set; }
    public string HomeAddressLine2 { get; set; }
    public Nullable<int> HomeSuburb { get; set; }
    public Nullable<int> LoanPurposeID { get; set; }
    public Nullable<bool> HasProperty { get; set; }
    public Nullable<int> CallStatusID { get; set; }
    public Nullable<System.DateTime> StatusStartDate { get; set; }
    public Nullable<System.DateTime> StatusExpiryDate { get; set; }
    public string StatusDetails { get; set; }
    public string StatusEnteredBy { get; set; }
    public Nullable<System.DateTime> StatusDate { get; set; }
    public Nullable<System.DateTime> ConversionDate { get; set; }
    public string ConversionBy { get; set; }
    public Nullable<int> ProspectID { get; set; }
    public Nullable<int> EnquiryID { get; set; }
    public Nullable<int> BorrowerID { get; set; }
    public string State { get; set; }
    public string HomeUnitNumber { get; set; }
    public string HomeStreetNumber { get; set; }
    public string HomeStreetName { get; set; }
    public string HomeStreetType { get; set; }
    public string ResidentialStatus { get; set; }
    public Nullable<bool> UnsecuredDebt { get; set; }
    public Nullable<bool> Income10to70 { get; set; }
    public Nullable<bool> BeenBankrupt { get; set; }
    public Nullable<bool> Initialised { get; set; }
    public Nullable<int> ApplicationNoDS { get; set; }
    public string Referral { get; set; }
    public string MMReferral { get; set; }
    public string ReferalURL { get; set; }
    public string SearchTerm { get; set; }
    public string Website { get; set; }
    public string ReferralType { get; set; }
    public Nullable<bool> SelfEmployed { get; set; }
    public Nullable<decimal> PaidWeeklyNet { get; set; }
    public Nullable<decimal> PaidWeeklyGross { get; set; }
    public Nullable<decimal> PaidFortnightlyNet { get; set; }
    public Nullable<decimal> PaidFortnightlyGross { get; set; }
    public Nullable<decimal> PaidMonthlyNet { get; set; }
    public Nullable<decimal> PaidMonthlyGross { get; set; }
    public Nullable<decimal> PaidYearlyNet { get; set; }
    public Nullable<decimal> PaidYearlyGross { get; set; }
    public Nullable<decimal> PaidWeeklyNetPartner { get; set; }
    public Nullable<decimal> PaidWeeklyGrossPartner { get; set; }
    public Nullable<decimal> PaidFortnightlyNetPartner { get; set; }
    public Nullable<decimal> PaidFortnightlyGrossPartner { get; set; }
    public Nullable<decimal> PaidMonthlyNetPartner { get; set; }
    public Nullable<decimal> PaidMonthlyGrossPartner { get; set; }
    public Nullable<decimal> PaidYearlyNetPartner { get; set; }
    public Nullable<decimal> PaidYearlyGrossPartner { get; set; }
    public Nullable<bool> DefaultOnCreditFileOrDebtInCollectionAgencyUnsure { get; set; }
    public Nullable<bool> C8am11am { get; set; }
    public string C8am11amAt { get; set; }
    public Nullable<bool> C11am2pm { get; set; }
    public string C11am2pmAt { get; set; }
    public Nullable<bool> C2pm5pm { get; set; }
    public string C2pm5pmAt { get; set; }
    public Nullable<bool> C5pm8pm { get; set; }
    public string C5pm8pmAt { get; set; }
    public string AdwordsKeyword { get; set; }
    public string Adgroup { get; set; }
    public Nullable<decimal> DebtAmount { get; set; }
    public Nullable<short> BeenBankruptOrDebtAgreement { get; set; }
    public string BeenBankruptOrDebtAgreementNote { get; set; }
    public Nullable<short> DebtSecuredOnCar { get; set; }
    public string DebtSecuredOnCarNote { get; set; }
    public Nullable<short> DebtSecuredOnEquipment { get; set; }
    public string DebtSecuredOnEquipmentNote { get; set; }
    public Nullable<short> DebtSecuredOnHouseholdGoods { get; set; }
    public string DebtSecuredOnHouseholdGoodsNote { get; set; }
    public Nullable<short> AllDebtInYourName { get; set; }
    public string AllDebtInYourNameNote { get; set; }
    public Nullable<short> LateInPaymentLast3MonthOrCCOverLimit { get; set; }
    public string LateInPaymentLast3MonthOrCCOverLimitNote { get; set; }
    public Nullable<short> DefaultOnCreditFileOrDebtInCollectionAgency { get; set; }
    public string DefaultOnCreditFileOrDebtInCollectionAgencyNote { get; set; }
    public Nullable<short> DifficultToMakeEndMeet { get; set; }
    public string DifficultToMakeEndMeetNote { get; set; }
    public string ApplyLoanAnyWhereYES { get; set; }
    public string ApplyLoanAnyWhereNO { get; set; }
    public Nullable<short> ApplyLoanAnyWhere { get; set; }
    public string ApplyLoanAnyWhereNote { get; set; }
    public Nullable<short> HaveOneCanLoan { get; set; }
    public string HaveOneCanLoanNote { get; set; }
    public Nullable<bool> TelephoneHomePrefer { get; set; }
    public Nullable<bool> TelephoneWorkPrefer { get; set; }
    public Nullable<bool> MobilePrefer { get; set; }
    public Nullable<bool> SMSPrefer { get; set; }
    public Nullable<bool> FaxPrefer { get; set; }
    public Nullable<bool> EmailPrefer { get; set; }
    public Nullable<bool> EmailSecondPrefer { get; set; }
    public Nullable<byte> CallNotInterestedReasonID { get; set; }
    public Nullable<byte> CallNoProductAvailableReasonID { get; set; }
    public Nullable<int> CompanyID { get; set; }
    public string IsDefault { get; set; }
    public string CFIssue01 { get; set; }
    public string CFIssue02 { get; set; }
    public string CFIssue03 { get; set; }
    public string CFIssue04 { get; set; }
    public string ProductID { get; set; }
    public Nullable<int> ApplicantIDCF { get; set; }
    public Nullable<short> DSBeenBankrupt { get; set; }
    public Nullable<short> DSIsStrugglingToPayDebt { get; set; }
    public Nullable<short> DSBeenDifficultToAffordLivingExpense { get; set; }
    public Nullable<short> DSHavingUnsecuredDebtOver10000 { get; set; }
    public string DSBeenBankruptNote { get; set; }
    public string DSIsStrugglingToPayDebtNote { get; set; }
    public string DSBeenDifficultToAffordLivingExpenseNote { get; set; }
    public string DSHavingUnsecuredDebtOver10000Note { get; set; }
    public string DSQualifierGeneralNote { get; set; }
    public string LoanPurpose { get; set; }

    public MMCompany Company { get; set; }

    public MMCall(Int32 ID)
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
      {
        StringBuilder sql = new StringBuilder();
        sql.Append(" SELECT  Call.*, LoanPurpose.*, [User].PhoneNumber AS OperatorPhoneNumber ");
        sql.Append(" FROM Call LEFT OUTER JOIN ");
        sql.Append(" LoanPurpose ON Call.LoanPurposeID = LoanPurpose.LoanPurposeID LEFT OUTER JOIN ");
        sql.Append(" [User] ON Call.Operator = [User].Username ");
        sql.Append(" WHERE CallID = " + ID.ToString());

        SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
        conn.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
          CallID = (dr["CallID"] as Int32?) ?? 0;
          GUID = Convert.ToString(dr["GUID"]);
          Title = Convert.ToString(dr["Title"]);
          Surname = Convert.ToString(dr["Surname"]);
          FirstName = Convert.ToString(dr["FirstName"]);
          Nickname = Convert.ToString(dr["Nickname"]);
          TelephoneHome = Convert.ToString(dr["TelephoneHome"]);
          TelephoneWork = Convert.ToString(dr["TelephoneWork"]);
          Mobile = Convert.ToString(dr["Mobile"]);
          Fax = Convert.ToString(dr["Fax"]);
          Email = Convert.ToString(dr["Email"]);
          EmailSecond = Convert.ToString(dr["EmailSecond"]);
          CallDate = Convert.ToDateTime(dr["CallDate"]);
          Operator = Convert.ToString(dr["Operator"]);
          OperatorPhoneNumber = Convert.ToString(dr["OperatorPhoneNumber"]);
          Notes = Convert.ToString(dr["Notes"]);
          HomeAddressLine1 = Convert.ToString(dr["HomeAddressLine1"]);
          HomeAddressLine2 = Convert.ToString(dr["HomeAddressLine2"]);
          HomeSuburb = (dr["HomeSuburb"] as Int32?) ?? null;
          LoanPurposeID = (dr["LoanPurposeID"] as Int32?) ?? null;
          HasProperty = (dr["HasProperty"] as Boolean?) ?? null;
          CallStatusID = (dr["CallStatusID"] as Int32?) ?? null;
          StatusStartDate = (dr["StatusStartDate"] as DateTime?) ?? null;
          StatusExpiryDate = (dr["StatusExpiryDate"] as DateTime?) ?? null;
          StatusDetails = Convert.ToString(dr["StatusDetails"]);
          StatusEnteredBy = Convert.ToString(dr["StatusEnteredBy"]);
          StatusDate = (dr["StatusDate"] as DateTime?) ?? null;
          ConversionDate = (dr["ConversionDate"] as DateTime?) ?? null;
          ConversionBy = Convert.ToString(dr["ConversionBy"]);
          ProspectID = (dr["ProspectID"] as Int32?) ?? null;
          EnquiryID = (dr["EnquiryID"] as Int32?) ?? null;
          BorrowerID = (dr["BorrowerID"] as Int32?) ?? null;
          State = Convert.ToString(dr["State"]);
          HomeUnitNumber = Convert.ToString(dr["HomeUnitNumber"]);
          HomeStreetNumber = Convert.ToString(dr["HomeStreetNumber"]);
          HomeStreetName = Convert.ToString(dr["HomeStreetName"]);
          HomeStreetType = Convert.ToString(dr["HomeStreetType"]);
          ResidentialStatus = Convert.ToString(dr["ResidentialStatus"]);
          UnsecuredDebt = (dr["UnsecuredDebt"] as Boolean?) ?? null;
          Income10to70 = (dr["Income10to70"] as Boolean?) ?? null;
          BeenBankrupt = (dr["BeenBankrupt"] as Boolean?) ?? null;
          Initialised = (dr["Initialised"] as Boolean?) ?? null;
          ApplicationNoDS = (dr["ApplicationNoDS"] as Int32?) ?? null;
          Referral = Convert.ToString(dr["Referral"]);
          MMReferral = Convert.ToString(dr["MMReferral"]);
          ReferalURL = Convert.ToString(dr["ReferalURL"]);
          SearchTerm = Convert.ToString(dr["SearchTerm"]);
          Website = Convert.ToString(dr["Website"]);
          ReferralType = Convert.ToString(dr["ReferralType"]);
          SelfEmployed = (dr["SelfEmployed"] as Boolean?) ?? null;
          PaidWeeklyNet = (dr["PaidWeeklyNet"] as Decimal?) ?? null;
          PaidWeeklyGross = (dr["PaidWeeklyGross"] as Decimal?) ?? null;
          PaidFortnightlyNet = (dr["PaidFortnightlyNet"] as Decimal?) ?? null;
          PaidFortnightlyGross = (dr["PaidFortnightlyGross"] as Decimal?) ?? null;
          PaidMonthlyNet = (dr["PaidMonthlyNet"] as Decimal?) ?? null;
          PaidMonthlyGross = (dr["PaidMonthlyGross"] as Decimal?) ?? null;
          PaidYearlyNet = (dr["PaidYearlyNet"] as Decimal?) ?? null;
          PaidYearlyGross = (dr["PaidYearlyGross"] as Decimal?) ?? null;
          PaidWeeklyNetPartner = (dr["PaidWeeklyNetPartner"] as Decimal?) ?? null;
          PaidWeeklyGrossPartner = (dr["PaidWeeklyGrossPartner"] as Decimal?) ?? null;
          PaidFortnightlyNetPartner = (dr["PaidFortnightlyNetPartner"] as Decimal?) ?? null;
          PaidFortnightlyGrossPartner = (dr["PaidFortnightlyGrossPartner"] as Decimal?) ?? null;
          PaidMonthlyNetPartner = (dr["PaidMonthlyNetPartner"] as Decimal?) ?? null;
          PaidMonthlyGrossPartner = (dr["PaidMonthlyGrossPartner"] as Decimal?) ?? null;
          PaidYearlyNetPartner = (dr["PaidYearlyNetPartner"] as Decimal?) ?? null;
          PaidYearlyGrossPartner = (dr["PaidYearlyGrossPartner"] as Decimal?) ?? null;
          DefaultOnCreditFileOrDebtInCollectionAgencyUnsure = (dr["DefaultOnCreditFileOrDebtInCollectionAgencyUnsure"] as Boolean?) ?? null;
          C8am11am = (dr["8am11am"] as Boolean?) ?? null;
          C8am11amAt = Convert.ToString(dr["8am11amAt"]);
          C11am2pm = (dr["11am2pm"] as Boolean?) ?? null;
          C11am2pmAt = Convert.ToString(dr["11am2pmAt"]);
          C2pm5pm = (dr["2pm5pm"] as Boolean?) ?? null;
          C2pm5pmAt = Convert.ToString(dr["2pm5pmAt"]);
          C5pm8pm = (dr["5pm8pm"] as Boolean?) ?? null;
          C5pm8pmAt = Convert.ToString(dr["5pm8pmAt"]);
          AdwordsKeyword = Convert.ToString(dr["AdwordsKeyword"]);
          Adgroup = Convert.ToString(dr["Adgroup"]);
          DebtAmount = (dr["DebtAmount"] as Decimal?) ?? null;
          BeenBankruptOrDebtAgreement = (dr["BeenBankruptOrDebtAgreement"] as Int16?) ?? null;
          BeenBankruptOrDebtAgreementNote = Convert.ToString(dr["BeenBankruptOrDebtAgreementNote"]);
          DebtSecuredOnCar = (dr["DebtSecuredOnCar"] as Int16?) ?? null;
          DebtSecuredOnCarNote = Convert.ToString(dr["DebtSecuredOnCarNote"]);
          DebtSecuredOnEquipment = (dr["DebtSecuredOnEquipment"] as Int16?) ?? null;
          DebtSecuredOnEquipmentNote = Convert.ToString(dr["DebtSecuredOnEquipmentNote"]);
          DebtSecuredOnHouseholdGoods = (dr["DebtSecuredOnHouseholdGoods"] as Int16?) ?? null;
          DebtSecuredOnHouseholdGoodsNote = Convert.ToString(dr["DebtSecuredOnHouseholdGoodsNote"]);
          AllDebtInYourName = (dr["AllDebtInYourName"] as Int16?) ?? null;
          AllDebtInYourNameNote = Convert.ToString(dr["AllDebtInYourNameNote"]);
          LateInPaymentLast3MonthOrCCOverLimit = (dr["LateInPaymentLast3MonthOrCCOverLimit"] as Int16?) ?? null;
          LateInPaymentLast3MonthOrCCOverLimitNote = Convert.ToString(dr["LateInPaymentLast3MonthOrCCOverLimitNote"]);
          DefaultOnCreditFileOrDebtInCollectionAgency = (dr["DefaultOnCreditFileOrDebtInCollectionAgency"] as Int16?) ?? null;
          DefaultOnCreditFileOrDebtInCollectionAgencyNote = Convert.ToString(dr["DefaultOnCreditFileOrDebtInCollectionAgencyNote"]);
          DifficultToMakeEndMeet = (dr["DifficultToMakeEndMeet"] as Int16?) ?? null;
          DifficultToMakeEndMeetNote = Convert.ToString(dr["DifficultToMakeEndMeetNote"]);
          ApplyLoanAnyWhereYES = Convert.ToString(dr["ApplyLoanAnyWhereYES"]);
          ApplyLoanAnyWhereNO = Convert.ToString(dr["ApplyLoanAnyWhereNO"]);
          ApplyLoanAnyWhere = (dr["ApplyLoanAnyWhere"] as Int16?) ?? null;
          ApplyLoanAnyWhereNote = Convert.ToString(dr["ApplyLoanAnyWhereNote"]);
          HaveOneCanLoan = (dr["HaveOneCanLoan"] as Int16?) ?? null;
          HaveOneCanLoanNote = Convert.ToString(dr["HaveOneCanLoanNote"]);
          TelephoneHomePrefer = (dr["TelephoneHomePrefer"] as Boolean?) ?? null;
          TelephoneWorkPrefer = (dr["TelephoneWorkPrefer"] as Boolean?) ?? null;
          MobilePrefer = (dr["MobilePrefer"] as Boolean?) ?? null;
          SMSPrefer = (dr["SMSPrefer"] as Boolean?) ?? null;
          FaxPrefer = (dr["FaxPrefer"] as Boolean?) ?? null;
          EmailPrefer = (dr["EmailPrefer"] as Boolean?) ?? null;
          EmailSecondPrefer = (dr["EmailSecondPrefer"] as Boolean?) ?? null;
          CallNotInterestedReasonID = (dr["CallNotInterestedReasonID"] as Byte?) ?? null;
          CallNoProductAvailableReasonID = (dr["CallNoProductAvailableReasonID"] as Byte?) ?? null;
          CompanyID = (dr["CompanyID"] as Int32?) ?? null;
          IsDefault = Convert.ToString(dr["IsDefault"]);
          CFIssue01 = Convert.ToString(dr["CFIssue01"]);
          CFIssue02 = Convert.ToString(dr["CFIssue02"]);
          CFIssue03 = Convert.ToString(dr["CFIssue03"]);
          CFIssue04 = Convert.ToString(dr["CFIssue04"]);
          ProductID = Convert.ToString(dr["ProductID"]);
          ApplicantIDCF = (dr["ApplicantIDCF"] as Int32?) ?? null;
          DSBeenBankrupt = (dr["DSBeenBankrupt"] as Int16?) ?? null;
          DSIsStrugglingToPayDebt = (dr["DSIsStrugglingToPayDebt"] as Int16?) ?? null;
          DSBeenDifficultToAffordLivingExpense = (dr["DSBeenDifficultToAffordLivingExpense"] as Int16?) ?? null;
          DSHavingUnsecuredDebtOver10000 = (dr["DSHavingUnsecuredDebtOver10000"] as Int16?) ?? null;
          DSBeenBankruptNote = Convert.ToString(dr["DSBeenBankruptNote"]);
          DSIsStrugglingToPayDebtNote = Convert.ToString(dr["DSIsStrugglingToPayDebtNote"]);
          DSBeenDifficultToAffordLivingExpenseNote = Convert.ToString(dr["DSBeenDifficultToAffordLivingExpenseNote"]);
          DSHavingUnsecuredDebtOver10000Note = Convert.ToString(dr["DSHavingUnsecuredDebtOver10000Note"]);
          DSQualifierGeneralNote = Convert.ToString(dr["DSQualifierGeneralNote"]);
          LoanPurpose = Convert.ToString(dr["LoanPurpose"]);
        }
      }
    }
    public void LoadCompany()
    {
      Company = new MMCompany(Convert.ToInt32(CompanyID));
    }
  }
}