﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EmailWebService.Models
{
	public class MMSecurity
	{
		public int SecurityID { get; set; }
		public Nullable<int> ApplicationNo { get; set; }
		public string SecurityDescription { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public Nullable<int> Suburb { get; set; }
		public Nullable<int> PropertyTypeID { get; set; }
		public Nullable<decimal> PurchasePrice { get; set; }
		public Nullable<decimal> EstimatedMarketValue { get; set; }
		public Nullable<decimal> CurrentMarketValueOfLand { get; set; }
		public Nullable<decimal> TenderPrice { get; set; }
		public string InsuranceArranged { get; set; }
		public Nullable<decimal> SumInsured { get; set; }
		public string FirstMortgageLender { get; set; }
		public string SecondMortgageLender { get; set; }
		public string ThridMortgageLender { get; set; }
		public Nullable<decimal> FirstMortgageOwing { get; set; }
		public Nullable<decimal> SecondMortgageOwing { get; set; }
		public Nullable<decimal> ThridMortgageOwing { get; set; }
		public Nullable<decimal> ActualMarketValue { get; set; }
		public string FourthMortgageLender { get; set; }
		public string FifthMortgageLender { get; set; }
		public Nullable<decimal> FourthMortgageOwing { get; set; }
		public Nullable<decimal> FifthMortgageOwing { get; set; }
		public string FirstMortgageAddress { get; set; }
		public string FirstMortgageACName { get; set; }
		public string FirstMortgageACNo { get; set; }
		public string FirstMortgageContactName { get; set; }
		public string FirstMortgageContactNumber { get; set; }
		public string SecondMortgageAddress { get; set; }
		public string SecondMortgageACName { get; set; }
		public string SecondMortgageACNo { get; set; }
		public string SecondMortgageContactName { get; set; }
		public string SecondMortgageContactNumber { get; set; }
		public string ThirdMortgageAddress { get; set; }
		public string ThirdMortgageACName { get; set; }
		public string ThirdMortgageACNo { get; set; }
		public string ThirdMortgageContactName { get; set; }
		public string ThirdMortgageContactNumber { get; set; }
		public string FourthMortgageAddress { get; set; }
		public string FourthMortgageACName { get; set; }
		public string FourthMortgageACNo { get; set; }
		public string FourthMortgageContactName { get; set; }
		public string FourthMortgageContactNumber { get; set; }
		public string FifthMortgageAddress { get; set; }
		public string FifthMortgageACName { get; set; }
		public string FifthMortgageACNo { get; set; }
		public string FifthMortgageContactName { get; set; }
		public string FifthMortgageContactNumber { get; set; }
		public Nullable<int> ProspectID { get; set; }
		public string UnitNumber { get; set; }
		public string StreetNumber { get; set; }
		public string StreetName { get; set; }
		public string StreetType { get; set; }
		public string SixthMortgageLender { get; set; }
		public Nullable<decimal> SixthMortgageOwing { get; set; }
		public string SixthMortgageAddress { get; set; }
		public string SixthMortgageACName { get; set; }
		public string SixthMortgageACNo { get; set; }
		public string SixthMortgageContactNumber { get; set; }
		public string SixthMortgageContactName { get; set; }
		public string SeventhMortgageLender { get; set; }
		public Nullable<decimal> SeventhMortgageOwing { get; set; }
		public string SeventhMortgageAddress { get; set; }
		public string SeventhMortgageACName { get; set; }
		public string SeventhMortgageACNo { get; set; }
		public string SeventhMortgageContactNumber { get; set; }
		public string SeventhMortgageContactName { get; set; }
		public string Lender { get; set; }
		public string County { get; set; }
		public string Parish { get; set; }
		public string TitleReference { get; set; }
		public string Lot { get; set; }
		public string Folio { get; set; }
		public string Plan { get; set; }
		public string LotB { get; set; }
		public string PlanB { get; set; }
		public string Volume { get; set; }
		public string FirstMortgageType { get; set; }
		public string SecondMortgageType { get; set; }
		public string ThirdMortgageType { get; set; }
		public string FourthMortgageType { get; set; }
		public string FifthMortgageType { get; set; }
		public string SixthMortgageType { get; set; }
		public string SeventhMortgageType { get; set; }
		public string Comment { get; set; }
		public string GoogleMap { get; set; }
		public string PopulationSearch { get; set; }
		public string GEMWorth { get; set; }
		public string QBE { get; set; }
		public string FirstMortgageNonApplicant { get; set; }
		public string SecondMortgageNonApplicant { get; set; }
		public string ThirdMortgageNonApplicant { get; set; }
		public string FourthMortgageNonApplicant { get; set; }
		public string FifthMortgageNonApplicant { get; set; }
		public string SixthMortgageNonApplicant { get; set; }
		public string SeventhMortgageNonApplicant { get; set; }
		public string WALandDesc { get; set; }

		public string SuburbText { get; set; }

		public MMSecurity()
		{
			//default values (constructor)
			FirstMortgageType = "1st Mortgage";
			SecondMortgageType = "2nd Mortgage";
			ThirdMortgageType = "3rd Mortgage";
			FourthMortgageType = "Overdraft";
			FifthMortgageType = "Business Loan";
			SixthMortgageType = "Caveat 1";
			SeventhMortgageType = "Caveat 2";
		}

		public MMSecurity(int ID)
		{

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM Applicant WHERE ApplicantID = " + ID.ToString(), conn);
				conn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read())
				{
					SecurityID = ID;

					ApplicationNo = (dr["ApplicationNo"] as Int32?) ?? 0;
					SecurityDescription = Convert.ToString(dr["SecurityDescription"]);
					AddressLine1 = Convert.ToString(dr["AddressLine1"]);
					AddressLine2 = Convert.ToString(dr["AddressLine2"]);
					Suburb = (dr["Suburb"] as Int32?) ?? 0;
					PropertyTypeID = (dr["PropertyTypeID"] as Int32?) ?? 0;
					PurchasePrice = (dr["PurchasePrice"] as Decimal?) ?? 0;
					EstimatedMarketValue = (dr["EstimatedMarketValue"] as Decimal?) ?? 0;
					CurrentMarketValueOfLand = (dr["CurrentMarketValueOfLand"] as Decimal?) ?? 0;
					TenderPrice = (dr["TenderPrice"] as Decimal?) ?? 0;
					InsuranceArranged = Convert.ToString(dr["InsuranceArranged"]);
					SumInsured = (dr["SumInsured"] as Decimal?) ?? 0;
					FirstMortgageLender = Convert.ToString(dr["FirstMortgageLender"]);
					SecondMortgageLender = Convert.ToString(dr["SecondMortgageLender"]);
					ThridMortgageLender = Convert.ToString(dr["ThridMortgageLender"]);
					FirstMortgageOwing = (dr["FirstMortgageOwing"] as Decimal?) ?? 0;
					SecondMortgageOwing = (dr["SecondMortgageOwing"] as Decimal?) ?? 0;
					ThridMortgageOwing = (dr["ThridMortgageOwing"] as Decimal?) ?? 0;
					ActualMarketValue = (dr["ActualMarketValue"] as Decimal?) ?? 0;
					FourthMortgageLender = Convert.ToString(dr["FourthMortgageLender"]);
					FifthMortgageLender = Convert.ToString(dr["FifthMortgageLender"]);
					FourthMortgageOwing = (dr["FourthMortgageOwing"] as Decimal?) ?? 0;
					FifthMortgageOwing = (dr["FifthMortgageOwing"] as Decimal?) ?? 0;
					FirstMortgageAddress = Convert.ToString(dr["FirstMortgageAddress"]);
					FirstMortgageACName = Convert.ToString(dr["FirstMortgageACName"]);
					FirstMortgageACNo = Convert.ToString(dr["FirstMortgageACNo"]);
					FirstMortgageContactName = Convert.ToString(dr["FirstMortgageContactName"]);
					FirstMortgageContactNumber = Convert.ToString(dr["FirstMortgageContactNumber"]);
					SecondMortgageAddress = Convert.ToString(dr["SecondMortgageAddress"]);
					SecondMortgageACName = Convert.ToString(dr["SecondMortgageACName"]);
					SecondMortgageACNo = Convert.ToString(dr["SecondMortgageACNo"]);
					SecondMortgageContactName = Convert.ToString(dr["SecondMortgageContactName"]);
					SecondMortgageContactNumber = Convert.ToString(dr["SecondMortgageContactNumber"]);
					ThirdMortgageAddress = Convert.ToString(dr["ThirdMortgageAddress"]);
					ThirdMortgageACName = Convert.ToString(dr["ThirdMortgageACName"]);
					ThirdMortgageACNo = Convert.ToString(dr["ThirdMortgageACNo"]);
					ThirdMortgageContactName = Convert.ToString(dr["ThirdMortgageContactName"]);
					ThirdMortgageContactNumber = Convert.ToString(dr["ThirdMortgageContactNumber"]);
					FourthMortgageAddress = Convert.ToString(dr["FourthMortgageAddress"]);
					FourthMortgageACName = Convert.ToString(dr["FourthMortgageACName"]);
					FourthMortgageACNo = Convert.ToString(dr["FourthMortgageACNo"]);
					FourthMortgageContactName = Convert.ToString(dr["FourthMortgageContactName"]);
					FourthMortgageContactNumber = Convert.ToString(dr["FourthMortgageContactNumber"]);
					FifthMortgageAddress = Convert.ToString(dr["FifthMortgageAddress"]);
					FifthMortgageACName = Convert.ToString(dr["FifthMortgageACName"]);
					FifthMortgageACNo = Convert.ToString(dr["FifthMortgageACNo"]);
					FifthMortgageContactName = Convert.ToString(dr["FifthMortgageContactName"]);
					FifthMortgageContactNumber = Convert.ToString(dr["FifthMortgageContactNumber"]);
					ProspectID = (dr["ProspectID"] as Int32?) ?? 0;
					UnitNumber = Convert.ToString(dr["UnitNumber"]);
					StreetNumber = Convert.ToString(dr["StreetNumber"]);
					StreetName = Convert.ToString(dr["StreetName"]);
					StreetType = Convert.ToString(dr["StreetType"]);
					SixthMortgageLender = Convert.ToString(dr["SixthMortgageLender"]);
					SixthMortgageOwing = (dr["SixthMortgageOwing"] as Decimal?) ?? 0;
					SixthMortgageAddress = Convert.ToString(dr["SixthMortgageAddress"]);
					SixthMortgageACName = Convert.ToString(dr["SixthMortgageACName"]);
					SixthMortgageACNo = Convert.ToString(dr["SixthMortgageACNo"]);
					SixthMortgageContactNumber = Convert.ToString(dr["SixthMortgageContactNumber"]);
					SixthMortgageContactName = Convert.ToString(dr["SixthMortgageContactName"]);
					SeventhMortgageLender = Convert.ToString(dr["SeventhMortgageLender"]);
					SeventhMortgageOwing = (dr["SeventhMortgageOwing"] as Decimal?) ?? 0;
					SeventhMortgageAddress = Convert.ToString(dr["SeventhMortgageAddress"]);
					SeventhMortgageACName = Convert.ToString(dr["SeventhMortgageACName"]);
					SeventhMortgageACNo = Convert.ToString(dr["SeventhMortgageACNo"]);
					SeventhMortgageContactNumber = Convert.ToString(dr["SeventhMortgageContactNumber"]);
					SeventhMortgageContactName = Convert.ToString(dr["SeventhMortgageContactName"]);
					Lender = Convert.ToString(dr["Lender"]);
					County = Convert.ToString(dr["County"]);
					Parish = Convert.ToString(dr["Parish"]);
					TitleReference = Convert.ToString(dr["TitleReference"]);
					Lot = Convert.ToString(dr["Lot"]);
					Folio = Convert.ToString(dr["Folio"]);
					Plan = Convert.ToString(dr["Plan"]);
					LotB = Convert.ToString(dr["LotB"]);
					PlanB = Convert.ToString(dr["PlanB"]);
					Volume = Convert.ToString(dr["Volume"]);
					FirstMortgageType = Convert.ToString(dr["FirstMortgageType"]);
					SecondMortgageType = Convert.ToString(dr["SecondMortgageType"]);
					ThirdMortgageType = Convert.ToString(dr["ThirdMortgageType"]);
					FourthMortgageType = Convert.ToString(dr["FourthMortgageType"]);
					FifthMortgageType = Convert.ToString(dr["FifthMortgageType"]);
					SixthMortgageType = Convert.ToString(dr["SixthMortgageType"]);
					SeventhMortgageType = Convert.ToString(dr["SeventhMortgageType"]);
					Comment = Convert.ToString(dr["Comment"]);
					GoogleMap = Convert.ToString(dr["GoogleMap"]);
					PopulationSearch = Convert.ToString(dr["PopulationSearch"]);
					GEMWorth = Convert.ToString(dr["GEMWorth"]);
					QBE = Convert.ToString(dr["QBE"]);
					FirstMortgageNonApplicant = Convert.ToString(dr["FirstMortgageNonApplicant"]);
					SecondMortgageNonApplicant = Convert.ToString(dr["SecondMortgageNonApplicant"]);
					ThirdMortgageNonApplicant = Convert.ToString(dr["ThirdMortgageNonApplicant"]);
					FourthMortgageNonApplicant = Convert.ToString(dr["FourthMortgageNonApplicant"]);
					FifthMortgageNonApplicant = Convert.ToString(dr["FifthMortgageNonApplicant"]);
					SixthMortgageNonApplicant = Convert.ToString(dr["SixthMortgageNonApplicant"]);
					SeventhMortgageNonApplicant = Convert.ToString(dr["SeventhMortgageNonApplicant"]);
					WALandDesc = Convert.ToString(dr["WALandDesc"]);

					SuburbText = new MMPCode(Convert.ToInt32(Suburb)).SuburbStatePostCode;


				}
			}

		}
		public void Save()
		{
			//building the UPDATE string...
			StringBuilder sql = new StringBuilder();
			sql.Append("UPDATE Security SET ");


			sql.Append("ApplicationNo = @ApplicationNo, ");
			sql.Append("SecurityDescription = @SecurityDescription, ");
			sql.Append("AddressLine1 = @AddressLine1, ");
			sql.Append("AddressLine2 = @AddressLine2, ");
			sql.Append("Suburb = @Suburb, ");
			sql.Append("PropertyTypeID = @PropertyTypeID, ");
			sql.Append("PurchasePrice = @PurchasePrice, ");
			sql.Append("EstimatedMarketValue = @EstimatedMarketValue, ");
			sql.Append("CurrentMarketValueOfLand = @CurrentMarketValueOfLand, ");
			sql.Append("TenderPrice = @TenderPrice, ");
			sql.Append("InsuranceArranged = @InsuranceArranged, ");
			sql.Append("SumInsured = @SumInsured, ");
			sql.Append("FirstMortgageLender = @FirstMortgageLender, ");
			sql.Append("SecondMortgageLender = @SecondMortgageLender, ");
			sql.Append("ThridMortgageLender = @ThridMortgageLender, ");
			sql.Append("FirstMortgageOwing = @FirstMortgageOwing, ");
			sql.Append("SecondMortgageOwing = @SecondMortgageOwing, ");
			sql.Append("ThridMortgageOwing = @ThridMortgageOwing, ");
			sql.Append("ActualMarketValue = @ActualMarketValue, ");
			sql.Append("FourthMortgageLender = @FourthMortgageLender, ");
			sql.Append("FifthMortgageLender = @FifthMortgageLender, ");
			sql.Append("FourthMortgageOwing = @FourthMortgageOwing, ");
			sql.Append("FifthMortgageOwing = @FifthMortgageOwing, ");
			sql.Append("FirstMortgageAddress = @FirstMortgageAddress, ");
			sql.Append("FirstMortgageACName = @FirstMortgageACName, ");
			sql.Append("FirstMortgageACNo = @FirstMortgageACNo, ");
			sql.Append("FirstMortgageContactName = @FirstMortgageContactName, ");
			sql.Append("FirstMortgageContactNumber = @FirstMortgageContactNumber, ");
			sql.Append("SecondMortgageAddress = @SecondMortgageAddress, ");
			sql.Append("SecondMortgageACName = @SecondMortgageACName, ");
			sql.Append("SecondMortgageACNo = @SecondMortgageACNo, ");
			sql.Append("SecondMortgageContactName = @SecondMortgageContactName, ");
			sql.Append("SecondMortgageContactNumber = @SecondMortgageContactNumber, ");
			sql.Append("ThirdMortgageAddress = @ThirdMortgageAddress, ");
			sql.Append("ThirdMortgageACName = @ThirdMortgageACName, ");
			sql.Append("ThirdMortgageACNo = @ThirdMortgageACNo, ");
			sql.Append("ThirdMortgageContactName = @ThirdMortgageContactName, ");
			sql.Append("ThirdMortgageContactNumber = @ThirdMortgageContactNumber, ");
			sql.Append("FourthMortgageAddress = @FourthMortgageAddress, ");
			sql.Append("FourthMortgageACName = @FourthMortgageACName, ");
			sql.Append("FourthMortgageACNo = @FourthMortgageACNo, ");
			sql.Append("FourthMortgageContactName = @FourthMortgageContactName, ");
			sql.Append("FourthMortgageContactNumber = @FourthMortgageContactNumber, ");
			sql.Append("FifthMortgageAddress = @FifthMortgageAddress, ");
			sql.Append("FifthMortgageACName = @FifthMortgageACName, ");
			sql.Append("FifthMortgageACNo = @FifthMortgageACNo, ");
			sql.Append("FifthMortgageContactName = @FifthMortgageContactName, ");
			sql.Append("FifthMortgageContactNumber = @FifthMortgageContactNumber, ");
			sql.Append("ProspectID = @ProspectID, ");
			sql.Append("UnitNumber = @UnitNumber, ");
			sql.Append("StreetNumber = @StreetNumber, ");
			sql.Append("StreetName = @StreetName, ");
			sql.Append("StreetType = @StreetType, ");
			sql.Append("SixthMortgageLender = @SixthMortgageLender, ");
			sql.Append("SixthMortgageOwing = @SixthMortgageOwing, ");
			sql.Append("SixthMortgageAddress = @SixthMortgageAddress, ");
			sql.Append("SixthMortgageACName = @SixthMortgageACName, ");
			sql.Append("SixthMortgageACNo = @SixthMortgageACNo, ");
			sql.Append("SixthMortgageContactNumber = @SixthMortgageContactNumber, ");
			sql.Append("SixthMortgageContactName = @SixthMortgageContactName, ");
			sql.Append("SeventhMortgageLender = @SeventhMortgageLender, ");
			sql.Append("SeventhMortgageOwing = @SeventhMortgageOwing, ");
			sql.Append("SeventhMortgageAddress = @SeventhMortgageAddress, ");
			sql.Append("SeventhMortgageACName = @SeventhMortgageACName, ");
			sql.Append("SeventhMortgageACNo = @SeventhMortgageACNo, ");
			sql.Append("SeventhMortgageContactNumber = @SeventhMortgageContactNumber, ");
			sql.Append("SeventhMortgageContactName = @SeventhMortgageContactName, ");
			sql.Append("Lender = @Lender, ");
			sql.Append("County = @County, ");
			sql.Append("Parish = @Parish, ");
			sql.Append("TitleReference = @TitleReference, ");
			sql.Append("Lot = @Lot, ");
			sql.Append("Folio = @Folio, ");
			sql.Append("[Plan] = @Plan, ");
			sql.Append("LotB = @LotB, ");
			sql.Append("PlanB = @PlanB, ");
			sql.Append("Volume = @Volume, ");
			sql.Append("FirstMortgageType = @FirstMortgageType, ");
			sql.Append("SecondMortgageType = @SecondMortgageType, ");
			sql.Append("ThirdMortgageType = @ThirdMortgageType, ");
			sql.Append("FourthMortgageType = @FourthMortgageType, ");
			sql.Append("FifthMortgageType = @FifthMortgageType, ");
			sql.Append("SixthMortgageType = @SixthMortgageType, ");
			sql.Append("SeventhMortgageType = @SeventhMortgageType, ");
			sql.Append("Comment = @Comment, ");
			sql.Append("GoogleMap = @GoogleMap, ");
			sql.Append("PopulationSearch = @PopulationSearch, ");
			sql.Append("GEMWorth = @GEMWorth, ");
			sql.Append("QBE = @QBE, ");
			sql.Append("FirstMortgageNonApplicant = @FirstMortgageNonApplicant, ");
			sql.Append("SecondMortgageNonApplicant = @SecondMortgageNonApplicant, ");
			sql.Append("ThirdMortgageNonApplicant = @ThirdMortgageNonApplicant, ");
			sql.Append("FourthMortgageNonApplicant = @FourthMortgageNonApplicant, ");
			sql.Append("FifthMortgageNonApplicant = @FifthMortgageNonApplicant, ");
			sql.Append("SixthMortgageNonApplicant = @SixthMortgageNonApplicant, ");
			sql.Append("SeventhMortgageNonApplicant = @SeventhMortgageNonApplicant, ");
			sql.Append("WALandDesc = @WALandDesc, ");

			sql.Append("WHERE ApplicantID = @ApplicantID");

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand(sql.ToString(), conn);


				cmd.Parameters.Add(new SqlParameter("SecurityID", ((object)SecurityID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ApplicationNo", ((object)ApplicationNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecurityDescription", ((object)SecurityDescription ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AddressLine1", ((object)AddressLine1 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("AddressLine2", ((object)AddressLine2 ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Suburb", ((object)Suburb ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PropertyTypeID", ((object)PropertyTypeID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PurchasePrice", ((object)PurchasePrice ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("EstimatedMarketValue", ((object)EstimatedMarketValue ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("CurrentMarketValueOfLand", ((object)CurrentMarketValueOfLand ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TenderPrice", ((object)TenderPrice ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("InsuranceArranged", ((object)InsuranceArranged ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SumInsured", ((object)SumInsured ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageLender", ((object)FirstMortgageLender ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageLender", ((object)SecondMortgageLender ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThridMortgageLender", ((object)ThridMortgageLender ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageOwing", ((object)FirstMortgageOwing ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageOwing", ((object)SecondMortgageOwing ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThridMortgageOwing", ((object)ThridMortgageOwing ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ActualMarketValue", ((object)ActualMarketValue ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageLender", ((object)FourthMortgageLender ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageLender", ((object)FifthMortgageLender ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageOwing", ((object)FourthMortgageOwing ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageOwing", ((object)FifthMortgageOwing ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageAddress", ((object)FirstMortgageAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageACName", ((object)FirstMortgageACName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageACNo", ((object)FirstMortgageACNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageContactName", ((object)FirstMortgageContactName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageContactNumber", ((object)FirstMortgageContactNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageAddress", ((object)SecondMortgageAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageACName", ((object)SecondMortgageACName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageACNo", ((object)SecondMortgageACNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageContactName", ((object)SecondMortgageContactName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageContactNumber", ((object)SecondMortgageContactNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThirdMortgageAddress", ((object)ThirdMortgageAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThirdMortgageACName", ((object)ThirdMortgageACName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThirdMortgageACNo", ((object)ThirdMortgageACNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThirdMortgageContactName", ((object)ThirdMortgageContactName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThirdMortgageContactNumber", ((object)ThirdMortgageContactNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageAddress", ((object)FourthMortgageAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageACName", ((object)FourthMortgageACName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageACNo", ((object)FourthMortgageACNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageContactName", ((object)FourthMortgageContactName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageContactNumber", ((object)FourthMortgageContactNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageAddress", ((object)FifthMortgageAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageACName", ((object)FifthMortgageACName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageACNo", ((object)FifthMortgageACNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageContactName", ((object)FifthMortgageContactName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageContactNumber", ((object)FifthMortgageContactNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ProspectID", ((object)ProspectID ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("UnitNumber", ((object)UnitNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StreetNumber", ((object)StreetNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StreetName", ((object)StreetName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("StreetType", ((object)StreetType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageLender", ((object)SixthMortgageLender ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageOwing", ((object)SixthMortgageOwing ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageAddress", ((object)SixthMortgageAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageACName", ((object)SixthMortgageACName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageACNo", ((object)SixthMortgageACNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageContactNumber", ((object)SixthMortgageContactNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageContactName", ((object)SixthMortgageContactName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageLender", ((object)SeventhMortgageLender ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageOwing", ((object)SeventhMortgageOwing ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageAddress", ((object)SeventhMortgageAddress ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageACName", ((object)SeventhMortgageACName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageACNo", ((object)SeventhMortgageACNo ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageContactNumber", ((object)SeventhMortgageContactNumber ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageContactName", ((object)SeventhMortgageContactName ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Lender", ((object)Lender ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("County", ((object)County ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Parish", ((object)Parish ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("TitleReference", ((object)TitleReference ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Lot", ((object)Lot ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Folio", ((object)Folio ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Plan", ((object)Plan ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("LotB", ((object)LotB ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PlanB", ((object)PlanB ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Volume", ((object)Volume ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageType", ((object)FirstMortgageType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageType", ((object)SecondMortgageType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThirdMortgageType", ((object)ThirdMortgageType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageType", ((object)FourthMortgageType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageType", ((object)FifthMortgageType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageType", ((object)SixthMortgageType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageType", ((object)SeventhMortgageType ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("Comment", ((object)Comment ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("GoogleMap", ((object)GoogleMap ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("PopulationSearch", ((object)PopulationSearch ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("GEMWorth", ((object)GEMWorth ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("QBE", ((object)QBE ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FirstMortgageNonApplicant", ((object)FirstMortgageNonApplicant ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SecondMortgageNonApplicant", ((object)SecondMortgageNonApplicant ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("ThirdMortgageNonApplicant", ((object)ThirdMortgageNonApplicant ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FourthMortgageNonApplicant", ((object)FourthMortgageNonApplicant ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("FifthMortgageNonApplicant", ((object)FifthMortgageNonApplicant ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SixthMortgageNonApplicant", ((object)SixthMortgageNonApplicant ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("SeventhMortgageNonApplicant", ((object)SeventhMortgageNonApplicant ?? DBNull.Value)));
				cmd.Parameters.Add(new SqlParameter("WALandDesc", ((object)WALandDesc ?? DBNull.Value)));

				conn.Open();
				cmd.ExecuteNonQuery();
			}
		}

		public void Create()
		{
			//checking that we have an ApplicationNo
			if ((ApplicationNo != null) && (ApplicationNo > 0))
			{

				//building the INSERT string...
				StringBuilder sql = new StringBuilder();
				sql.Append("INSERT INTO Securities (");

				sql.Append("ApplicationNo, ");
				sql.Append("SecurityDescription, ");
				sql.Append("AddressLine1, ");
				sql.Append("AddressLine2, ");
				sql.Append("Suburb, ");
				sql.Append("PropertyTypeID, ");
				sql.Append("PurchasePrice, ");
				sql.Append("EstimatedMarketValue, ");
				sql.Append("CurrentMarketValueOfLand, ");
				sql.Append("TenderPrice, ");
				sql.Append("InsuranceArranged, ");
				sql.Append("SumInsured, ");
				sql.Append("FirstMortgageLender, ");
				sql.Append("SecondMortgageLender, ");
				sql.Append("ThridMortgageLender, ");
				sql.Append("FirstMortgageOwing, ");
				sql.Append("SecondMortgageOwing, ");
				sql.Append("ThridMortgageOwing, ");
				sql.Append("ActualMarketValue, ");
				sql.Append("FourthMortgageLender, ");
				sql.Append("FifthMortgageLender, ");
				sql.Append("FourthMortgageOwing, ");
				sql.Append("FifthMortgageOwing, ");
				sql.Append("FirstMortgageAddress, ");
				sql.Append("FirstMortgageACName, ");
				sql.Append("FirstMortgageACNo, ");
				sql.Append("FirstMortgageContactName, ");
				sql.Append("FirstMortgageContactNumber, ");
				sql.Append("SecondMortgageAddress, ");
				sql.Append("SecondMortgageACName, ");
				sql.Append("SecondMortgageACNo, ");
				sql.Append("SecondMortgageContactName, ");
				sql.Append("SecondMortgageContactNumber, ");
				sql.Append("ThirdMortgageAddress, ");
				sql.Append("ThirdMortgageACName, ");
				sql.Append("ThirdMortgageACNo, ");
				sql.Append("ThirdMortgageContactName, ");
				sql.Append("ThirdMortgageContactNumber, ");
				sql.Append("FourthMortgageAddress, ");
				sql.Append("FourthMortgageACName, ");
				sql.Append("FourthMortgageACNo, ");
				sql.Append("FourthMortgageContactName, ");
				sql.Append("FourthMortgageContactNumber, ");
				sql.Append("FifthMortgageAddress, ");
				sql.Append("FifthMortgageACName, ");
				sql.Append("FifthMortgageACNo, ");
				sql.Append("FifthMortgageContactName, ");
				sql.Append("FifthMortgageContactNumber, ");
				sql.Append("ProspectID, ");
				sql.Append("UnitNumber, ");
				sql.Append("StreetNumber, ");
				sql.Append("StreetName, ");
				sql.Append("StreetType, ");
				sql.Append("SixthMortgageLender, ");
				sql.Append("SixthMortgageOwing, ");
				sql.Append("SixthMortgageAddress, ");
				sql.Append("SixthMortgageACName, ");
				sql.Append("SixthMortgageACNo, ");
				sql.Append("SixthMortgageContactNumber, ");
				sql.Append("SixthMortgageContactName, ");
				sql.Append("SeventhMortgageLender, ");
				sql.Append("SeventhMortgageOwing, ");
				sql.Append("SeventhMortgageAddress, ");
				sql.Append("SeventhMortgageACName, ");
				sql.Append("SeventhMortgageACNo, ");
				sql.Append("SeventhMortgageContactNumber, ");
				sql.Append("SeventhMortgageContactName, ");
				sql.Append("Lender, ");
				sql.Append("County, ");
				sql.Append("Parish, ");
				sql.Append("TitleReference, ");
				sql.Append("Lot, ");
				sql.Append("Folio, ");
				sql.Append("[Plan], ");
				sql.Append("LotB, ");
				sql.Append("PlanB, ");
				sql.Append("Volume, ");
				sql.Append("FirstMortgageType, ");
				sql.Append("SecondMortgageType, ");
				sql.Append("ThirdMortgageType, ");
				sql.Append("FourthMortgageType, ");
				sql.Append("FifthMortgageType, ");
				sql.Append("SixthMortgageType, ");
				sql.Append("SeventhMortgageType, ");
				sql.Append("Comment, ");
				sql.Append("GoogleMap, ");
				sql.Append("PopulationSearch, ");
				sql.Append("GEMWorth, ");
				sql.Append("QBE, ");
				sql.Append("FirstMortgageNonApplicant, ");
				sql.Append("SecondMortgageNonApplicant, ");
				sql.Append("ThirdMortgageNonApplicant, ");
				sql.Append("FourthMortgageNonApplicant, ");
				sql.Append("FifthMortgageNonApplicant, ");
				sql.Append("SixthMortgageNonApplicant, ");
				sql.Append("SeventhMortgageNonApplicant, ");
				sql.Append("WALandDesc ");

				sql.Append(") VALUES (");

				sql.Append("@ApplicationNo, ");
				sql.Append("@SecurityDescription, ");
				sql.Append("@AddressLine1, ");
				sql.Append("@AddressLine2, ");
				sql.Append("@Suburb, ");
				sql.Append("@PropertyTypeID, ");
				sql.Append("@PurchasePrice, ");
				sql.Append("@EstimatedMarketValue, ");
				sql.Append("@CurrentMarketValueOfLand, ");
				sql.Append("@TenderPrice, ");
				sql.Append("@InsuranceArranged, ");
				sql.Append("@SumInsured, ");
				sql.Append("@FirstMortgageLender, ");
				sql.Append("@SecondMortgageLender, ");
				sql.Append("@ThridMortgageLender, ");
				sql.Append("@FirstMortgageOwing, ");
				sql.Append("@SecondMortgageOwing, ");
				sql.Append("@ThridMortgageOwing, ");
				sql.Append("@ActualMarketValue, ");
				sql.Append("@FourthMortgageLender, ");
				sql.Append("@FifthMortgageLender, ");
				sql.Append("@FourthMortgageOwing, ");
				sql.Append("@FifthMortgageOwing, ");
				sql.Append("@FirstMortgageAddress, ");
				sql.Append("@FirstMortgageACName, ");
				sql.Append("@FirstMortgageACNo, ");
				sql.Append("@FirstMortgageContactName, ");
				sql.Append("@FirstMortgageContactNumber, ");
				sql.Append("@SecondMortgageAddress, ");
				sql.Append("@SecondMortgageACName, ");
				sql.Append("@SecondMortgageACNo, ");
				sql.Append("@SecondMortgageContactName, ");
				sql.Append("@SecondMortgageContactNumber, ");
				sql.Append("@ThirdMortgageAddress, ");
				sql.Append("@ThirdMortgageACName, ");
				sql.Append("@ThirdMortgageACNo, ");
				sql.Append("@ThirdMortgageContactName, ");
				sql.Append("@ThirdMortgageContactNumber, ");
				sql.Append("@FourthMortgageAddress, ");
				sql.Append("@FourthMortgageACName, ");
				sql.Append("@FourthMortgageACNo, ");
				sql.Append("@FourthMortgageContactName, ");
				sql.Append("@FourthMortgageContactNumber, ");
				sql.Append("@FifthMortgageAddress, ");
				sql.Append("@FifthMortgageACName, ");
				sql.Append("@FifthMortgageACNo, ");
				sql.Append("@FifthMortgageContactName, ");
				sql.Append("@FifthMortgageContactNumber, ");
				sql.Append("@ProspectID, ");
				sql.Append("@UnitNumber, ");
				sql.Append("@StreetNumber, ");
				sql.Append("@StreetName, ");
				sql.Append("@StreetType, ");
				sql.Append("@SixthMortgageLender, ");
				sql.Append("@SixthMortgageOwing, ");
				sql.Append("@SixthMortgageAddress, ");
				sql.Append("@SixthMortgageACName, ");
				sql.Append("@SixthMortgageACNo, ");
				sql.Append("@SixthMortgageContactNumber, ");
				sql.Append("@SixthMortgageContactName, ");
				sql.Append("@SeventhMortgageLender, ");
				sql.Append("@SeventhMortgageOwing, ");
				sql.Append("@SeventhMortgageAddress, ");
				sql.Append("@SeventhMortgageACName, ");
				sql.Append("@SeventhMortgageACNo, ");
				sql.Append("@SeventhMortgageContactNumber, ");
				sql.Append("@SeventhMortgageContactName, ");
				sql.Append("@Lender, ");
				sql.Append("@County, ");
				sql.Append("@Parish, ");
				sql.Append("@TitleReference, ");
				sql.Append("@Lot, ");
				sql.Append("@Folio, ");
				sql.Append("@Plan, ");
				sql.Append("@LotB, ");
				sql.Append("@PlanB, ");
				sql.Append("@Volume, ");
				sql.Append("@FirstMortgageType, ");
				sql.Append("@SecondMortgageType, ");
				sql.Append("@ThirdMortgageType, ");
				sql.Append("@FourthMortgageType, ");
				sql.Append("@FifthMortgageType, ");
				sql.Append("@SixthMortgageType, ");
				sql.Append("@SeventhMortgageType, ");
				sql.Append("@Comment, ");
				sql.Append("@GoogleMap, ");
				sql.Append("@PopulationSearch, ");
				sql.Append("@GEMWorth, ");
				sql.Append("@QBE, ");
				sql.Append("@FirstMortgageNonApplicant, ");
				sql.Append("@SecondMortgageNonApplicant, ");
				sql.Append("@ThirdMortgageNonApplicant, ");
				sql.Append("@FourthMortgageNonApplicant, ");
				sql.Append("@FifthMortgageNonApplicant, ");
				sql.Append("@SixthMortgageNonApplicant, ");
				sql.Append("@SeventhMortgageNonApplicant, ");
				sql.Append("@WALandDesc ");

				sql.Append("); SELECT @@IDENTITY");

				using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
				{
					SqlCommand cmd = new SqlCommand(sql.ToString(), conn);



					cmd.Parameters.Add(new SqlParameter("ApplicationNo", ((object)ApplicationNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecurityDescription", ((object)SecurityDescription ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AddressLine1", ((object)AddressLine1 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("AddressLine2", ((object)AddressLine2 ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Suburb", ((object)Suburb ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PropertyTypeID", ((object)PropertyTypeID ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PurchasePrice", ((object)PurchasePrice ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("EstimatedMarketValue", ((object)EstimatedMarketValue ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("CurrentMarketValueOfLand", ((object)CurrentMarketValueOfLand ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TenderPrice", ((object)TenderPrice ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("InsuranceArranged", ((object)InsuranceArranged ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SumInsured", ((object)SumInsured ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageLender", ((object)FirstMortgageLender ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageLender", ((object)SecondMortgageLender ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThridMortgageLender", ((object)ThridMortgageLender ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageOwing", ((object)FirstMortgageOwing ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageOwing", ((object)SecondMortgageOwing ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThridMortgageOwing", ((object)ThridMortgageOwing ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ActualMarketValue", ((object)ActualMarketValue ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageLender", ((object)FourthMortgageLender ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageLender", ((object)FifthMortgageLender ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageOwing", ((object)FourthMortgageOwing ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageOwing", ((object)FifthMortgageOwing ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageAddress", ((object)FirstMortgageAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageACName", ((object)FirstMortgageACName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageACNo", ((object)FirstMortgageACNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageContactName", ((object)FirstMortgageContactName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageContactNumber", ((object)FirstMortgageContactNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageAddress", ((object)SecondMortgageAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageACName", ((object)SecondMortgageACName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageACNo", ((object)SecondMortgageACNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageContactName", ((object)SecondMortgageContactName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageContactNumber", ((object)SecondMortgageContactNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThirdMortgageAddress", ((object)ThirdMortgageAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThirdMortgageACName", ((object)ThirdMortgageACName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThirdMortgageACNo", ((object)ThirdMortgageACNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThirdMortgageContactName", ((object)ThirdMortgageContactName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThirdMortgageContactNumber", ((object)ThirdMortgageContactNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageAddress", ((object)FourthMortgageAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageACName", ((object)FourthMortgageACName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageACNo", ((object)FourthMortgageACNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageContactName", ((object)FourthMortgageContactName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageContactNumber", ((object)FourthMortgageContactNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageAddress", ((object)FifthMortgageAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageACName", ((object)FifthMortgageACName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageACNo", ((object)FifthMortgageACNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageContactName", ((object)FifthMortgageContactName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageContactNumber", ((object)FifthMortgageContactNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ProspectID", ((object)ProspectID ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("UnitNumber", ((object)UnitNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("StreetNumber", ((object)StreetNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("StreetName", ((object)StreetName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("StreetType", ((object)StreetType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageLender", ((object)SixthMortgageLender ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageOwing", ((object)SixthMortgageOwing ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageAddress", ((object)SixthMortgageAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageACName", ((object)SixthMortgageACName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageACNo", ((object)SixthMortgageACNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageContactNumber", ((object)SixthMortgageContactNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageContactName", ((object)SixthMortgageContactName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageLender", ((object)SeventhMortgageLender ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageOwing", ((object)SeventhMortgageOwing ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageAddress", ((object)SeventhMortgageAddress ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageACName", ((object)SeventhMortgageACName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageACNo", ((object)SeventhMortgageACNo ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageContactNumber", ((object)SeventhMortgageContactNumber ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageContactName", ((object)SeventhMortgageContactName ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Lender", ((object)Lender ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("County", ((object)County ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Parish", ((object)Parish ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("TitleReference", ((object)TitleReference ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Lot", ((object)Lot ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Folio", ((object)Folio ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Plan", ((object)Plan ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("LotB", ((object)LotB ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PlanB", ((object)PlanB ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Volume", ((object)Volume ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageType", ((object)FirstMortgageType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageType", ((object)SecondMortgageType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThirdMortgageType", ((object)ThirdMortgageType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageType", ((object)FourthMortgageType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageType", ((object)FifthMortgageType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageType", ((object)SixthMortgageType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageType", ((object)SeventhMortgageType ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("Comment", ((object)Comment ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("GoogleMap", ((object)GoogleMap ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("PopulationSearch", ((object)PopulationSearch ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("GEMWorth", ((object)GEMWorth ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("QBE", ((object)QBE ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FirstMortgageNonApplicant", ((object)FirstMortgageNonApplicant ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SecondMortgageNonApplicant", ((object)SecondMortgageNonApplicant ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("ThirdMortgageNonApplicant", ((object)ThirdMortgageNonApplicant ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FourthMortgageNonApplicant", ((object)FourthMortgageNonApplicant ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("FifthMortgageNonApplicant", ((object)FifthMortgageNonApplicant ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SixthMortgageNonApplicant", ((object)SixthMortgageNonApplicant ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("SeventhMortgageNonApplicant", ((object)SeventhMortgageNonApplicant ?? DBNull.Value)));
					cmd.Parameters.Add(new SqlParameter("WALandDesc", ((object)WALandDesc ?? DBNull.Value)));
					conn.Open();
					try
					{
						SecurityID = Convert.ToInt32(cmd.ExecuteScalar());
					}
					catch { throw new Exception(); }
				}
			}
			else { throw new Exception("ApplicationNo is required to create."); }
		}
	}
}