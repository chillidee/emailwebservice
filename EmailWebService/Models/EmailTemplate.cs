﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using System.Net.Mail;
using EmailWebService.Models;
using Telerik.Web.UI;


namespace EmailWebService.Models
{
    public class EmailTemplate
    {
        private string _Body = string.Empty;
        private string _BodyText;
        private bool _Sent;
        private int _ID;
        private List<UploadedFileInfo> uploadedFiles = new List<UploadedFileInfo>();
        private string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("\\bin", "").Replace("file:\\", "") + "\\UploadedAttachments\\";
        private static DateTime _now = DateTime.Now;
        private string _timeStampString = (_now.Year.ToString().Substring(_now.Year.ToString().Length - 2)) + ("0" + _now.Month.ToString()).Substring(("0" + _now.Month.ToString()).Length - 2) + _now.Day.ToString() +
          "_" + ("0" + _now.Hour.ToString()).Substring(("0" + _now.Hour.ToString()).Length - 2) + ("0" + _now.Minute.ToString()).Substring(("0" + _now.Minute.ToString()).Length - 2);

        public string EmailID { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string QueryStringParameters { get; set; }
        public string Subject { get; set; }
        public Nullable<Int32> VariableStatusID { get; set; }
        public Nullable<Int32> CompanyID { get; set; }
        public Nullable<Int32> EventDefID { get; set; }
        public string Host { get; set; }
        public string Operator { get; set; }

        public string Body
        {
            get
            {
                if (string.IsNullOrEmpty(_Body))
                {
                    //lets attempt to read the file...
                    string path = ConfigurationManager.AppSettings["EmailTemplatePath"];
                    if (path == "")
                    {
                        path = HttpContext.Current.Request.PhysicalApplicationPath + "/" + EmailID;
                    }
                    else
                    {
                        path = path + "/" + EmailID;
                    }
                    StreamReader reader = new StreamReader(path + "/index.html");
                    _Body = reader.ReadToEnd();
                    reader.Dispose();
                    Host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
                      + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
                }
                return _Body;
            }
            set
            {
                _Body = value;
            }
        }
        public string BodyText
        {
            get
            {
                try
                {
                    _BodyText = ExtractString(Body, "BodyText");
                    return _BodyText;
                }
                catch
                {
                    return "#Error#. [BodyText] [/BodyText] tags were not found in the template email. Please contact the Marketing Department to have this rectified.";
                }

            }
            set
            {
                _BodyText = value;
                ReplaceBodyText();
            }
        }
        public MailMessage MailMessage { get; set; }
        public bool Sent { get { return _Sent; } }
        public string Firstname { get; set; }
        public string EmailAddress { get; set; }
        public string EmailAddressSecond { get; set; }
        public MMCall Call { get; set; }
        public MMApplication Application { get; set; }
        public CCApplicant CCApplication { get; set; }
        public BMAssetLiability BMAssetLiability { get; set; }
        public BMApplicant BMApplicant { get; set; }
        public List<Telerik.Web.UI.UploadedFileInfo> UploadedFiles
        {
            get { return uploadedFiles; }
            set { uploadedFiles = value; }
        }
        public string Folder { get; set; }
        public string Filename { get; set; }

        public EmailTemplate(string emailID, int ID)
        {
            _ID = ID;
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM EmailTemplate WHERE EmailID = @ID", conn);
                cmd.Parameters.Add(new SqlParameter("@ID", emailID));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    EmailID = Convert.ToString(dr["EmailID"]);
                    Type = Convert.ToString(dr["Type"]);
                    Description = Convert.ToString(dr["Description"]);
                    QueryStringParameters = Convert.ToString(dr["QueryStringParameters"]);
                    Subject = Convert.ToString(dr["Subject"]);
                    VariableStatusID = (dr["VariableStatusID"] as Int32?) ?? null;
                    CompanyID = (dr["CompanyID"] as Int32?) ?? null;
                    EventDefID = (dr["EventDefID"] as Int32?) ?? null;
                }
            }
            switch (Type)
            {
                case "LEAD":
                    Call = new MMCall(_ID);
                    Firstname = Call.FirstName;
                    EmailAddress = Call.Email;
                    EmailAddressSecond = Call.EmailSecond;
                    break;
                case "APPLICATION":
                    Application = new MMApplication(_ID);
                    Application.LoadApplicants();
                    Firstname = Application.MMApplicant1.FirstName;
                    EmailAddress = Application.MMApplicant1.Email;
                    break;
                case "CC_APPLICATION":
                    CCApplication = new CCApplicant(_ID);
                    CCApplication.LoadCompany();
                    Firstname = CCApplication.FirstName;
                    EmailAddress = CCApplication.Email;
                    break;
                case "WEB_ENQUIRY":
                    break;
                case "BM_CREDITOR_LETTER":
                    BMAssetLiability = new BMAssetLiability(_ID);
                    Firstname = BMAssetLiability.CreditorContactName;
                    EmailAddress = BMAssetLiability.CreditorEmail1;

                    BMAssetLiability.LoadBMApplicant();
                    _Body = Body;
                    FurnishBody_BMAssetLiability(BMAssetLiability);
                    Body = _Body;
                    Folder = BMAssetLiability.BMApplicant.ApplicantID.ToString() + " " + BMAssetLiability.BMApplicant.Surname
                      + @"\Creditors\[" + BMAssetLiability.BMAssetLiabilityID.ToString() + "] " + BMAssetLiability.CreditorName + " - " + Utils.CleanFileName(BMAssetLiability.AccountLoanNumber);
                    Filename = emailID.ToString() + "_" + _timeStampString + ".htm";

                    break;
                case "BM_APPLICATION":
                    BMApplicant = new BMApplicant(_ID);
                    Firstname = BMApplicant.FirstName;
                    EmailAddress = BMApplicant.Email;
                    _Body = Body;
                    FurnishBody_BMApplicant(BMApplicant);
                    Body = _Body;
                    Folder = _ID.ToString() + " " + BMApplicant.Surname;
                    Filename = emailID.ToString() + "_" + _timeStampString + ".htm";
                    break;
                default:
                    break;
            }
        }

        public void PrepareMailMessage(bool EmailSecond)
        {
            MMUser Owner;
            MailMessage m = new MailMessage();
            MailAddress emailFrom;
            string emailDomain;
            switch (Type)
            {
                case "LEAD":
                    MMCall c = new MMCall(_ID);
                    c.LoadCompany();
                    FurnishBody_MMCall(c);
                    Owner = new MMUser(c.Operator);
                    m = new MailMessage();

                    emailDomain = c.Company.WebsiteURL.Replace("www.", "");
                    if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }

                    //m.From = new MailAddress("do-not-reply@" + emailDomain);


                    if (!string.IsNullOrEmpty(Owner.Firstname) && !string.IsNullOrEmpty(Owner.Lastname))
                    {
                        if (EmailID.ToUpper() == "CC_CONTACTFIRST" || EmailID.ToUpper() == "CC_CONTACTSECOND" || EmailID.ToUpper() == "CC_CONTACTTHIRD")
                        {
                            emailFrom = new MailAddress("sales" + "@" + c.Company.WebsiteURL.Replace("www.", ""), Owner.Firstname + " " + Owner.Lastname);
                        }
                        else
                        {
                            emailFrom = new MailAddress(Owner.Firstname.ToLower() + "@" + c.Company.WebsiteURL.Replace("www.", ""), Owner.Firstname + " " + Owner.Lastname);
                            //m.CC.Add(emailFrom);
                        }
                        m.From = emailFrom;                        
                    }
                    else
                    {
                        throw new Exception("Lead owner is missing firstname/surname. Please see Sassy Suzanna.");
                    }

                    //m.To.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
                    //m.To.Add(new MailAddress("kelvin.ong@castmonkey.com"));
                    if (!EmailSecond)
                    {
                        m.To.Add(new MailAddress(c.Email, c.FirstName + " " + c.Surname));
                    }
                    else
                    {
                        m.To.Add(new MailAddress(c.EmailSecond, c.FirstName + " " + c.Surname));
                    }
                    //if (!string.IsNullOrEmpty(Owner.Email)) m.Bcc.Add(new MailAddress(Owner.Email, c.Operator));
                    m.Subject = Subject.Replace("[Firstname]", c.FirstName);
                    m.IsBodyHtml = true;
                    m.Body = Body;                    
                    //m.Body = "Hello there, from actual page!";
                    MailMessage = m;
                    break;
                case "APPLICATION":
                    /*MMApplication a = new MMApplication(ID);
                    a.LoadApplicants();
                    a.LoadCompany();
                    FurnishBody_MMApplication(et.EmailID, a);*/
                    MMApplication a = new MMApplication(_ID);
                    a.LoadCompany();
                    a.LoadApplicants();
                    FurnishBody_MMApplication(a);
                    Owner = new MMUser(a.SalesManager);
                    m = new MailMessage();

                    emailDomain = a.Company.WebsiteURL.Replace("www.", "");
                    if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }
                    //m.From = new MailAddress("do-not-reply@" + emailDomain);

                    if (!string.IsNullOrEmpty(Owner.Firstname) && !string.IsNullOrEmpty(Owner.Lastname))
                    {
                        emailFrom = new MailAddress(Owner.Firstname.ToLower() + "@" + a.Company.WebsiteURL.Replace("www.", ""), Owner.Firstname + " " + Owner.Lastname);
                        m.From = emailFrom;
                        //m.CC.Add(emailFrom);
                    }
                    else
                    {
                        throw new Exception("Application owner (Sales Manager) is missing firstname/surname. Please see Manager.");
                    }


                    m.To.Add(new MailAddress(a.MMApplicant1.Email, a.MMApplicant1.FirstName + " " + a.MMApplicant1.Surname));
                    //if (!string.IsNullOrEmpty(Owner.Email)) m.Bcc.Add(new MailAddress(Owner.Email, a.SalesManager));
                    m.Subject = Subject.Replace("[Firstname]", a.MMApplicant1.FirstName);
                    m.IsBodyHtml = true;


                    //testing attachments
                    //m.Attachments.Add(new Attachment(path + "folders.png"));
                    if (uploadedFiles.Count > 0)
                    {

                        foreach (UploadedFileInfo file in uploadedFiles)
                        {
                            try
                            {
                                m.Attachments.Add(new Attachment(path + file.FileName));
                                Body = Body + " " + file.FileName;
                            }
                            catch
                            { throw new Exception("Something went wrong with attaching files. The path is: " + path + " and the file is:" + file.FileName); }
                        }
                    }
                    m.Body = Body;//+ " " + path;
                    MailMessage = m;
                    break;
                case "CC_APPLICATION":

                    CCApplicant cc = new CCApplicant(_ID);
                    cc.LoadCompany();

                    FurnishBody_CCApplicant(cc);

                    Owner = new MMUser(cc.ApplicationManager);
                    m = new MailMessage();

                    emailDomain = cc.Company.WebsiteURL.Replace("www.", "");
                    if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }

                    if (!string.IsNullOrEmpty(Owner.Firstname) && !string.IsNullOrEmpty(Owner.Lastname))
                    {
                        emailFrom = new MailAddress(Owner.Firstname.ToLower() + "@" + cc.Company.WebsiteURL.Replace("www.", ""), Owner.Firstname + " " + Owner.Lastname);
                        m.From = emailFrom;
                        //m.CC.Add(emailFrom);
                    }
                    else
                    {
                        throw new Exception("Application owner is missing firstname/surname. Please see Manager.");
                    }

                    //m.To.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
                    //m.To.Add(new MailAddress("kelvin.ong@castmonkey.com"));
                    m.To.Add(new MailAddress(cc.Email, cc.FirstName + " " + cc.Surname));
                    //if (!string.IsNullOrEmpty(Owner.Email)) m.Bcc.Add(new MailAddress(Owner.Email, a.SalesManager));
                    m.Subject = Subject.Replace("[Firstname]", cc.FirstName);
                    m.IsBodyHtml = true;


                    //testing attachments
                    //m.Attachments.Add(new Attachment(path + "folders.png"));

                    //special case for CCDR Application packs...
                    if (EmailID == "CDR_Application_Pack")
                    {
                        string pdf_path = ConfigurationManager.AppSettings["EmailTemplatePath"] + "\\" + EmailID;
                        m.Attachments.Add(new Attachment(pdf_path + "\\CCDR Statement of Financal Position.pdf"));
                        m.Attachments.Add(new Attachment(pdf_path + "\\Clean Credit Debt Resolutions Application form.pdf"));
                    }


                    if (uploadedFiles.Count > 0)
                    {

                        foreach (UploadedFileInfo file in uploadedFiles)
                        {
                            try
                            {
                                m.Attachments.Add(new Attachment(path + file.FileName));
                                Body = Body + " " + file.FileName;
                            }
                            catch
                            { throw new Exception("Something went wrong with attaching files. The path is: " + path + " and the file is:" + file.FileName); }
                        }
                    }
                    m.Body = Body;//+ " " + path;
                    MailMessage = m;
                    break;

                case "BM_CREDITOR_LETTER":

                    BMAssetLiability bmal = new BMAssetLiability(_ID);
                    bmal.LoadBMApplicant();

                    FurnishBody_BMAssetLiability(bmal);

                    //Owner = new MMUser(cc.ApplicationManager);
                    m = new MailMessage();

                    emailDomain = "justbudget.com.au";
                    if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }


                    emailFrom = new MailAddress("creditors@justbudget.com.au", "Just Budget");
                    m.From = emailFrom;
                    //m.CC.Add(emailFrom);


                    //m.To.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
                    //m.To.Add(new MailAddress("kelvin.ong@castmonkey.com"));
                    m.To.Add(new MailAddress(bmal.CreditorEmail1, bmal.CreditorContactName));
                    if (!string.IsNullOrEmpty(bmal.CreditorEmail2))
                    {
                        m.CC.Add(new MailAddress(bmal.CreditorEmail2));
                    }
                    //if (!string.IsNullOrEmpty(Owner.Email)) m.Bcc.Add(new MailAddress(Owner.Email, a.SalesManager));
                    m.Subject = Subject.Replace("[ClientFullName]", bmal.BMApplicant.FirstName + " " + bmal.BMApplicant.Surname).Replace("[AccountNumber]", bmal.AccountLoanNumber);
                    m.IsBodyHtml = true;


                    //testing attachments
                    //m.Attachments.Add(new Attachment(path + "folders.png"));

                    //special case for CCDR Application packs...


                    if (uploadedFiles.Count > 0)
                    {

                        foreach (UploadedFileInfo file in uploadedFiles)
                        {
                            try
                            {
                                m.Attachments.Add(new Attachment(path + file.FileName));
                                Body = Body + " " + file.FileName;
                            }
                            catch
                            { throw new Exception("Something went wrong with attaching files. The path is: " + path + " and the file is:" + file.FileName); }
                        }
                    }
                    m.Body = Body;//+ " " + path;
                    MailMessage = m;
                    break;

                case "BM_APPLICATION":

                    BMApplicant ba = new BMApplicant(_ID);

                    FurnishBody_BMApplicant(ba);

                    m = new MailMessage();

                    emailDomain = "justbudget.com.au";
                    if (string.IsNullOrEmpty(emailDomain)) { emailDomain = "hatpacks.com.au"; }


                    emailFrom = new MailAddress("info@justbudget.com.au", "Just Budget");
                    m.From = emailFrom;
                    //m.CC.Add(emailFrom);

                    //m.To.Add(new MailAddress("kelvin@australianlendingcentre.com.au"));
                    //m.To.Add(new MailAddress("kelvin.ong@castmonkey.com"));
                    m.To.Add(new MailAddress(ba.Email, (ba.FirstName + ' ' + ba.Surname).Trim()));

                    //if (!string.IsNullOrEmpty(Owner.Email)) m.Bcc.Add(new MailAddress(Owner.Email, a.SalesManager));
                    m.Subject = Subject.Replace("[Firstname]", ba.FirstName + " " + ba.Surname);
                    m.IsBodyHtml = true;


                    //testing attachments
                    //m.Attachments.Add(new Attachment(path + "folders.png"));

                    //special case for JB Application packs...
                    if (EmailID == "JB_Application_Pack")
                    {
                        string pdf_path = ConfigurationManager.AppSettings["EmailTemplatePath"] + "\\" + EmailID;
                        if (ba.SalesPackNumber != null)
                        {
                            if (ba.SalesPackNumber > 0)
                            { //m.Attachments.Add(new Attachment(pdf_path + "\\Just Budget Application Form " + ba.SalesPackNumber.ToString() + ".pdf"));
                            }
                        }

                    }


                    if (uploadedFiles.Count > 0)
                    {

                        foreach (UploadedFileInfo file in uploadedFiles)
                        {
                            try
                            {
                                m.Attachments.Add(new Attachment(path + file.FileName));
                                Body = Body + " " + file.FileName;
                            }
                            catch
                            { throw new Exception("Something went wrong with attaching files. The path is: " + path + " and the file is:" + file.FileName); }
                        }
                    }
                    m.Body = Body;//+ " " + path;
                    MailMessage = m;
                    break;


                case "WEB_ENQUIRY":
                    break;
                default:
                    break;
            }
        }

        private void FurnishBody_MMCall(MMCall c)
        {

            string host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
              + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
            _Body = _Body.Replace("[Firstname]", c.FirstName);
            _Body = _Body.Replace("[Date]", DateTime.Now.ToString("dd-MMM-yyyy"));
            _Body = _Body.Replace("[Year]", DateTime.Now.ToString("yyyy"));
            _Body = _Body.Replace("[Owner]", c.Operator);
            _Body = _Body.Replace("[OwnerPhoneNumber]", c.OperatorPhoneNumber);            
            if (!string.IsNullOrEmpty(c.Operator))
                _Body = _Body.Replace("[OwnerImage]", c.Operator.Replace(" ", "-"));            
            _Body = _Body.Replace("[CallID]", c.CallID.ToString());
            _Body = _Body.Replace("[LoanPurpose]", c.LoanPurpose);
            _Body = _Body.Replace("[GooglePLUS]", c.Company.GooglePlusURL);
            _Body = _Body.Replace("src=\"images", "src=\"" + host + "/Emails/" + EmailID + "/images");
            _Body = _Body.Replace("src=\"Images", "src=\"" + host + "/Emails/" + EmailID + "/images");
            _Body = _Body.Replace("[is-app]", "false");
            _Body = _Body.Replace("[guid]", c.GUID);            
        }

        private void FurnishBody_MMApplication(MMApplication a)
        {
            string host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
              + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
            _Body = _Body.Replace("[Firstname]", a.MMApplicant1.FirstName);
            _Body = _Body.Replace("[Date]", DateTime.Now.ToString("dd-MMM-yyyy"));
            _Body = _Body.Replace("[Year]", DateTime.Now.ToString("yyyy"));
            _Body = _Body.Replace("src=\"images", "src=\"" + host + "/Emails/" + EmailID + "/images");
            _Body = _Body.Replace("src=\"Images", "src=\"" + host + "/Emails/" + EmailID + "/images");

            _Body = _Body.Replace("[CompanyName]", a.Company.Company);
            _Body = _Body.Replace("[CompanyABN]", a.Company.ABN);
            _Body = _Body.Replace("[CompanyPhone]", a.Company.Phone);
            _Body = _Body.Replace("[CompanyFax]", a.Company.Fax);
            _Body = _Body.Replace("[CompanyEmail]", a.Company.Email);
            _Body = _Body.Replace("[CompanyAccountName]", a.Company.AccountName);
            _Body = _Body.Replace("[CompanyAccountBSB]", a.Company.AccountBSB);
            _Body = _Body.Replace("[CompanyAccountNo]", a.Company.AccountNo);
            _Body = _Body.Replace("[CompanyCreditLicense]", a.Company.CreditLicNo);
            _Body = _Body.Replace("[FaceBookURL]", "http://" + a.Company.FacebookURL.Replace("http://", ""));
            _Body = _Body.Replace("[TwitterURL]", "http://" + a.Company.TwitterURL.Replace("http://", ""));
            _Body = _Body.Replace("[YouTubeURL]", "http://" + a.Company.YouTubeURL.Replace("http://", ""));
            _Body = _Body.Replace("[WebsiteURL]", "http://" + a.Company.WebsiteURL.Replace("http://", ""));
            _Body = _Body.Replace("[AboutUsURL]", "http://" + a.Company.AboutUsURL.Replace("http://", ""));
            _Body = _Body.Replace("[ContactUsURL]", "http://" + a.Company.ContactUsURL.Replace("http://", ""));
            _Body = _Body.Replace("[LinkedInURL]", "http://" + a.Company.LinkedInURL.Replace("http://", ""));
            _Body = _Body.Replace("[GooglePlusURL]", "http://" + a.Company.GooglePlusURL.Replace("http://", ""));
            _Body = _Body.Replace("[Logo]", host + "/images/" + a.Company.WebApplicationLogoImageFilename);
            _Body = _Body.Replace("[SalesManager]", a.SalesManager);            
            _Body = _Body.Replace("[CheckList-OutstandingItems]", a.CheckList_Outstanding);

        }

        private void FurnishBody_CCApplicant(CCApplicant c)
        {
            string host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
              + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
            _Body = _Body.Replace("[Firstname]", c.FirstName);
            _Body = _Body.Replace("[Date]", DateTime.Now.ToString("dd-MMM-yyyy"));
            _Body = _Body.Replace("[Year]", DateTime.Now.ToString("yyyy"));
            _Body = _Body.Replace("src=\"images", "src=\"" + host + "/Emails/" + EmailID + "/images");
            _Body = _Body.Replace("src=\"Images", "src=\"" + host + "/Emails/" + EmailID + "/images");

            _Body = _Body.Replace("[CompanyName]", c.Company.Company);
            _Body = _Body.Replace("[CompanyABN]", c.Company.ABN);
            _Body = _Body.Replace("[CompanyPhone]", c.Company.Phone);
            _Body = _Body.Replace("[CompanyFax]", c.Company.Fax);
            _Body = _Body.Replace("[CompanyEmail]", c.Company.Email);
            _Body = _Body.Replace("[CompanyAccountName]", c.Company.AccountName);
            _Body = _Body.Replace("[CompanyAccountBSB]", c.Company.AccountBSB);
            _Body = _Body.Replace("[CompanyAccountNo]", c.Company.AccountNo);
            _Body = _Body.Replace("[CompanyCreditLicense]", c.Company.CreditLicNo);
            _Body = _Body.Replace("[FaceBookURL]", "http://" + c.Company.FacebookURL.Replace("http://", ""));
            _Body = _Body.Replace("[TwitterURL]", "http://" + c.Company.TwitterURL.Replace("http://", ""));
            _Body = _Body.Replace("[YouTubeURL]", "http://" + c.Company.YouTubeURL.Replace("http://", ""));
            _Body = _Body.Replace("[WebsiteURL]", "http://" + c.Company.WebsiteURL.Replace("http://", ""));
            _Body = _Body.Replace("[AboutUsURL]", "http://" + c.Company.AboutUsURL.Replace("http://", ""));
            _Body = _Body.Replace("[ContactUsURL]", "http://" + c.Company.ContactUsURL.Replace("http://", ""));
            _Body = _Body.Replace("[LinkedInURL]", "http://" + c.Company.LinkedInURL.Replace("http://", ""));
            _Body = _Body.Replace("[GooglePlusURL]", "http://" + c.Company.GooglePlusURL.Replace("http://", ""));
            _Body = _Body.Replace("[Logo]", host + "/images/" + c.Company.WebApplicationLogoImageFilename);
            _Body = _Body.Replace("[SalesManager]", c.ApplicationManager);
            _Body = _Body.Replace("[CheckList-OutstandingItems]", c.CheckList_Outstanding);

        }

        private void FurnishBody_BMAssetLiability(BMAssetLiability b)
        {
            string host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
              + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);

            _Body = _Body.Replace("src=\"images", "src=\"" + host + "/Emails/" + EmailID + "/images");
            _Body = _Body.Replace("src=\"Images", "src=\"" + host + "/Emails/" + EmailID + "/images");

            b.LoadBMApplicant();

            _Body = _Body.Replace("[TOTAL_DEBT]", string.Format("{0:C}", b.BMApplicant.TotalBudgetToManage));
            _Body = _Body.Replace("[DEBT]", string.Format("{0:C}", b.TotalAmountOwing));
            _Body = _Body.Replace("[AccountNumber]", b.AccountLoanNumber);
            _Body = _Body.Replace("[PaymentCommencingMMMM]", b.PaymentCommencingMMMM);
            _Body = _Body.Replace("[PaymentCommencingYYYY]", b.PaymentCommencingYYYY);

            _Body = _Body.Replace("[MoratoriumTermSought]", b.MoratoriumTermSought.ToString());
            _Body = _Body.Replace("[CreateDate]", string.Format("{0:dd-MMM-yyyy}", b.BMApplicant.CreateDate));
            _Body = _Body.Replace("[Manager]", b.BMApplicant.ApplicationManager);

            _Body = _Body.Replace("[SURPLUS]", string.Format("{0:C}", (b.BMApplicant.AgreedActualSurplus - b.BMApplicant.MonthlyManagementFee)));
            _Body = _Body.Replace("[ClientFullName]", b.NamesOfAccount);
            _Body = _Body.Replace("[ClientFullAddress]", b.BMApplicant.FullAddress);

            _Body = _Body.Replace("[CREDITOR_SUMMARY_TABLE]", b.BMApplicant.html_debt_table);
            _Body = _Body.Replace("[CREDITOR_SUMMARY_TABLE_MINI]", b.BMApplicant.html_debt_table_mini);
            _Body = _Body.Replace("[CREDITOR_SUMMARY_TABLE_MICRO]", b.BMApplicant.html_debt_table_micro);

            _Body = _Body.Replace("[APPLICANT_ID]", b.BMApplicant.ApplicantID.ToString());



        }

        private void FurnishBody_BMApplicant(BMApplicant b)
        {
            string host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host
              + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);

            _Body = _Body.Replace("src=\"images", "src=\"" + host + "/Emails/" + EmailID + "/images");
            _Body = _Body.Replace("src=\"Images", "src=\"" + host + "/Emails/" + EmailID + "/images");


            _Body = _Body.Replace("[TOTAL_DEBT]", string.Format("{0:C}", b.TotalBudgetToManage));
            _Body = _Body.Replace("[SURPLUS]", string.Format("{0:C}", (b.AgreedActualSurplus - b.MonthlyManagementFee)));
            _Body = _Body.Replace("[Date]", DateTime.Now.ToString("dd-MMM-yyyy"));
            _Body = _Body.Replace("[Firstname]", b.FirstName);

            _Body = _Body.Replace("[Manager]", b.ApplicationManager);
            _Body = _Body.Replace("[CheckList-OutstandingItems]", b.CheckList_Outstanding);

            _Body = _Body.Replace("[CLIENTS_PAYMENT_START_DATE]", string.Format("{0:dd-MMM-yyyy}", b.PaymentFromClientStartDate));
            _Body = _Body.Replace("[AGREED_AMOUNT_TO_WITHDRAW]", string.Format("{0:C}", b.AgreedAmountToWithdrawFromClient));
            _Body = _Body.Replace("[WITHDRAWAL_FREQUENCY]", b.WithdrawalFrequency);
            _Body = _Body.Replace("[TOTAL_NUMBER_OF_MONTHS]", string.Format("{0:0.##}", b.NoOfMonths));
            _Body = _Body.Replace("[CREDITOR_SUMMARY_TABLE_MINI]", b.html_debt_table_mini);

            _Body = _Body.Replace("[is-app]", "true");
            _Body = _Body.Replace("[guid]", b.GUID);
            _Body = _Body.Replace("[CheckList-OutstandingItems]", b.CheckList_Outstanding);
        }


        public void SendEmail()
        {
            SmtpClient s = new SmtpClient("localhost");
            try
            {
                s.Send(MailMessage);
                _Sent = true;

                if (!String.IsNullOrEmpty(Folder))
                {
                    //now lets save the body as a .htm file, under the client folder...
                    using (ConnectUNCWithCredentials.UNCAccessWithCredentials unc = new ConnectUNCWithCredentials.UNCAccessWithCredentials())
                    {
                        if (unc.NetUseWithCredentials(ConfigurationManager.AppSettings["BM.ClientFolders.Path"], ConfigurationManager.AppSettings["BM.ClientFolders.Username"], ConfigurationManager.AppSettings["BM.ClientFolders.Domain"], ConfigurationManager.AppSettings["BM.ClientFolders.Password"]))
                        {
                            if (!Directory.Exists(ConfigurationManager.AppSettings["BM.ClientFolders.Path"] + @"\" + Folder))
                            {
                                Directory.CreateDirectory(ConfigurationManager.AppSettings["BM.ClientFolders.Path"] + @"\" + Folder);
                            }
                            using (StreamWriter sw = new StreamWriter(ConfigurationManager.AppSettings["BM.ClientFolders.Path"] + @"\" + Folder + @"\" + Filename))
                            {
                                sw.Write(MailMessage.Body);
                            }
                        }
                        else
                        {
                            throw new Exception("Unable to write to: " + ConfigurationManager.AppSettings["BM.ClientFolders.Path"] + @"\" + Folder + @"\" + Filename);
                        }
                    }
                }



                try
                {
                    if (BMAssetLiability != null)
                    {
                        BMAssetLiability.LogContact(Operator, Description, string.Empty, EventDefID);
                    }
                    else if (BMApplicant != null)
                    {
                        BMApplicant.LogContact(Operator, Description, string.Empty, EventDefID);
                    }
                }
                catch { }





                MailMessage.Attachments.Dispose();
                foreach (UploadedFileInfo file in uploadedFiles)
                {
                    try { System.IO.File.Delete(path + file.FileName); }
                    catch { throw new Exception("Something went wrong with deleting the temporary files for your attachements. The file is:" + path + file.FileName); }
                }
            }
            catch (Exception ex)
            {
                _Sent = false;
                throw ex;
            }
        }
        private string ExtractString(string s, string tag)
        {
            var startTag = "[" + tag + "]";
            int startIndex = s.IndexOf(startTag) + startTag.Length;
            int endIndex = s.IndexOf("[/" + tag + "]", startIndex);
            if (startIndex < 0 || endIndex < 0) { throw new Exception(); }
            return s.Substring(startIndex, endIndex - startIndex);
        }

        private void ReplaceBodyText()
        {

            int startIndex = Body.IndexOf("[BodyText]");
            int endIndex = _Body.IndexOf("[/BodyText]") + 11;//length of [/BodyText]
            string temp = _Body.Substring(startIndex, endIndex - startIndex);
            _Body = _Body.Replace(temp, _BodyText);

        }
    }
}

