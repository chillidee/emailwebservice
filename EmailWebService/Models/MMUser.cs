﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace EmailWebService.Models
{
	public class MMUser
	{
		public string Username { get; set; }
		public string Firstname { get; set; }
		public string Lastname { get; set; }
		public bool Active { get; set; }
		public System.DateTime CreateDate { get; set; }
		public Nullable<System.DateTime> DisableDate { get; set; }
		public bool IsSalesManager { get; set; }
		public string Initial { get; set; }
		public double Target_CostAgreementSent { get; set; }
		public double Target_ApplicationsToLender { get; set; }
		public double Target_BilledFees { get; set; }
		public double Target_ConvertedMortgageApplications { get; set; }
		public double Target_ConvertedDebtApplications { get; set; }
		public double Target_ConvertedCleanCreditApplications { get; set; }
		public bool ProcessingTeamMember { get; set; }
		public bool SalesTeamMember { get; set; }
		public Nullable<int> CompanyID { get; set; }
		public string Email { get; set; }
		public MMUser(string _Username)
		{
			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRISMARConnectionString"].ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("SELECT * FROM [User] WHERE Username = '" + _Username + "'", conn);
				conn.Open();
				SqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read())
				{
					Username = Convert.ToString(dr["Username"]);
					Firstname = Convert.ToString(dr["Firstname"]);
					Lastname = Convert.ToString(dr["Lastname"]);
					Active = (Boolean)dr["Active"];
					CreateDate = (dr["CreateDate"] as DateTime?) ?? DateTime.MinValue;
					DisableDate = (dr["DisableDate"] as DateTime?) ?? null;
					IsSalesManager = (Boolean)dr["IsSalesManager"];
					Initial = Convert.ToString(dr["Initial"]);
					Target_CostAgreementSent = (dr["Target_CostAgreementSent"] as Double?) ?? 0;
					Target_ApplicationsToLender = (dr["Target_ApplicationsToLender"] as Double?) ?? 0;
					Target_BilledFees = (dr["Target_BilledFees"] as Double?) ?? 0;
					Target_ConvertedMortgageApplications = (dr["Target_ConvertedMortgageApplications"] as Double?) ?? 0;
					Target_ConvertedDebtApplications = (dr["Target_ConvertedDebtApplications"] as Double?) ?? 0;
					Target_ConvertedCleanCreditApplications = (dr["Target_ConvertedCleanCreditApplications"] as Double?) ?? 0;
					ProcessingTeamMember = (Boolean)dr["ProcessingTeamMember"];
					SalesTeamMember = (Boolean)dr["SalesTeamMember"];
					CompanyID = (dr["CompanyID"] as Int32?) ?? null;
					Email = Convert.ToString(dr["Email"]);
				}
			}
		}
	}
}