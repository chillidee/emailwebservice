﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SendEmail.aspx.cs" Inherits="EmailWebService.SendEmail" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  <h1><asp:Literal runat="server" ID="litTitle" /></h1>
  <asp:Literal runat="server" ID="litRecipient" />
  <asp:Literal runat="server" ID="litFirstname" />
  <telerik:RadScriptManager runat="server" ID="rsm"></telerik:RadScriptManager>
  <telerik:RadEditor runat="server" ID="reBody" Height="600px" Skin="Simple" Width="960px" EditModes="Design">
    <CssFiles>
      <telerik:EditorCssFile Value="~/RadEditorStyle.css" />
    </CssFiles>
    <Tools>
      <telerik:EditorToolGroup Tag="MainToolbar">
        <telerik:EditorTool Name="FindAndReplace" />
        <telerik:EditorSeparator />
        <telerik:EditorSplitButton Name="Undo">
        </telerik:EditorSplitButton>
        <telerik:EditorSplitButton Name="Redo">
        </telerik:EditorSplitButton>
        <telerik:EditorSeparator />
        <telerik:EditorTool Name="Cut" />
        <telerik:EditorTool Name="Copy" />
        <telerik:EditorTool Name="Paste" ShortCut="CTRL+V" />
      </telerik:EditorToolGroup>
      <telerik:EditorToolGroup Tag="Formatting">
        <telerik:EditorTool Name="Bold" />
        <telerik:EditorTool Name="Italic" />
        <telerik:EditorTool Name="Underline" />
        <telerik:EditorSplitButton Name="ForeColor">
        </telerik:EditorSplitButton>
      </telerik:EditorToolGroup>
    </Tools>
    <Content>
    </Content>
    <TrackChangesSettings CanAcceptTrackChanges="False"></TrackChangesSettings>
  </telerik:RadEditor> 
  <script type="text/javascript">
    var $ = $telerik.$;
    var uploadsInProgress = 0;

    function onFileSelected(sender, args) {
      if (!uploadsInProgress)
        $("#SaveButton").attr("disabled", "disabled");

      uploadsInProgress++;

      var row = args.get_row();

      $(row).addClass("file-row");
    }

    function onFileUploaded(sender, args) {
      decrementUploadsInProgress();
    }

    function onUploadFailed(sender, args) {
      decrementUploadsInProgress();
    }

    function decrementUploadsInProgress() {
      uploadsInProgress--;

      if (!uploadsInProgress)
        $("#SaveButton").removeAttr("disabled");
    }
  </script>
  <asp:label runat="server" ID="lblAttachments">File Attachments:</asp:label>
  <telerik:RadAsyncUpload runat="server" ID="AsyncUpload"
                        HideFileInput="false"                        
                        MultipleFileSelection="Automatic"
                        AllowedFileExtensions=".jpeg,.jpg,.png,.docx,.xlsx,.doc,.xls,.pdf,.ppt,.pptx,.gif,.txt,.htm,.html" 
                        OnClientFileUploadFailed="onUploadFailed" OnClientFileSelected="onFileSelected"
                        OnClientFileUploaded="onFileUploaded"
                        TargetFolder="UploadedAttachments" 
                        EnableEmbeddedSkins="false" Height="40px"/>

  <asp:Literal runat="server" ID="litMessage" />
  <div style="text-align: right; padding: 10px">
	  <asp:LinkButton ID="btnCancel" Text="cancel" runat="server" CausesValidation="False" OnClientClick="window.close()"></asp:LinkButton>		
		<asp:Button ID="btnSend" Text="send" runat="server" CssClass="main" OnClick="btnSend_Click"></asp:Button>						
	</div>
  <asp:Label runat="server" ID="lblEmailID" Visible="false"/>
  <asp:Label runat="server" ID="lblID" Visible="false"/>
  <asp:Label runat="server" ID="lblEmailSecond" Visible="false"/>
  <asp:Label runat="server" ID="lblOperator" Visible="false"/>

</asp:Content>

