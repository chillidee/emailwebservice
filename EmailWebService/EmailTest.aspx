﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="EmailTest.aspx.cs" Inherits="EmailWebService.EmailTest" validateRequest="false"%>	

<form runat="server">
  <table>
  <tr>
    <td>From:</td><td><asp:TextBox runat="server" ID="tbFrom" Width="400px">do-not-reply@hatpacks.com.au</asp:TextBox></td>
  </tr>
  <tr>
     <td>To:</td><td><asp:TextBox runat="server" ID="tbTo" Width="400px"></asp:TextBox></td><td>Display name: (eg: Tim)</td><td><asp:TextBox runat="server" id="tbFromDisplayName" /></td>
  </tr>
  <tr>
    <td>Cc:</td><td><asp:TextBox runat="server" ID="tbCc" Width="400px"></asp:TextBox></td><td>Display name: (eg: Randy)</td><td><asp:TextBox runat="server" id="tbCCDisplayName" /></td>
  </tr>
  <tr>
	  <td>Subject:</td><td><asp:TextBox runat="server" ID="tbSubject" Width="400px">test</asp:TextBox></td>
  </tr>
  <tr>
    <td valign="top">Message:</td><td><asp:TextBox runat="server" id="tbMessage" TextMode="MultiLine" Height="300px" width="500px">test</asp:TextBox></td>
  </tr>
  <tr>
    <td></td>
	  <td><asp:Button runat="server" Text="send" ID="btnSend" OnClick="btnSend_Click" /></td>
  </tr>
</table>
</form>


