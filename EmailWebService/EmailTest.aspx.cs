﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

namespace EmailWebService
{
  public partial class EmailTest : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSend_Click(object sender, EventArgs e)
    {      
      MailMessage m = new MailMessage();
      m.From = new MailAddress(tbFrom.Text);      
      m.To.Add(new MailAddress(tbTo.Text, tbFromDisplayName.Text));
      if (!string.IsNullOrEmpty(tbCc.Text)) m.CC.Add(new MailAddress(tbCc.Text, tbCCDisplayName.Text));      
      m.Subject = tbSubject.Text;
      m.IsBodyHtml = true;
      m.Body = tbMessage.Text;

      SmtpClient s = new SmtpClient("localhost");
      m.Save(@"C:\Temp\testemail.eml");
      //s.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
      //s.PickupDirectoryLocation = @"C:\temp";
      s.Send(m);
    }
  }
}