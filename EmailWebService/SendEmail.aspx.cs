﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EmailWebService.Models;
using Telerik.Web.UI;
using System.Runtime.Serialization;


namespace EmailWebService
{
    public partial class SendEmail : System.Web.UI.Page
    {

        private List<Telerik.Web.UI.UploadedFileInfo> uploadedFiles = new List<Telerik.Web.UI.UploadedFileInfo>();
        public List<Telerik.Web.UI.UploadedFileInfo> UploadedFiles
        {
            get
            {
                return uploadedFiles;
            }
            set { uploadedFiles = value; }
        }
        /*private void PopulateUploadedFilesList()
        {
          foreach (UploadedFile file in AsyncUpload1.UploadedFiles)
          {
            UploadedFileInfo uploadedFileInfo = new UploadedFileInfo(file);
            UploadedFiles.Add(uploadedFileInfo);
          }
        }*/

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {
                    string EmailID;
                    string RecordID;
                    string EmailSecond;
                    string Operator;
                    string Application;
                    //checking required Querystrings...
                    EmailID = Request.QueryString["EmailID"];
                    RecordID = Request.QueryString["ID"];
                    EmailSecond = Request.QueryString["EmailSecond"];
                    Operator = Request.QueryString["op"];
                    Application = Request.QueryString["T"];

                    if (string.IsNullOrEmpty(EmailID))
                    {
                        Response.Write("EmailID required.");
                        Response.End();
                    }
                    else if (string.IsNullOrEmpty(RecordID))
                    {
                        Response.Write("ID required.");
                        Response.End();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Application) && Application == "CC")
                            EmailID = "CC_" + CCApplicant.GetLastIssueStatus(RecordID);

                        lblEmailID.Text = EmailID;
                        lblID.Text = RecordID;
                        lblEmailSecond.Text = EmailSecond;
                        lblOperator.Text = Operator;

                        EmailTemplate et = new EmailTemplate(EmailID, Convert.ToInt32(RecordID));
                        litTitle.Text = et.Description;
                        litFirstname.Text = "<h2 style=\"margin-bottom:10px\">Dear " + et.Firstname + ",</h2>";
                        litRecipient.Text = "Email will be sent to: " + ((EmailSecond == "1") ? et.EmailAddressSecond : et.EmailAddress) + "<br/>";
                        reBody.Content = et.BodyText;
                    }
                }
            }
            catch (Exception error)
            {
                Response.Write("ERROR - PLEASE contact Marketing.<br/>" + error.Message + " - " + error.StackTrace);
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            string EmailID = lblEmailID.Text;
            string RecordID = lblID.Text;
            string EmailSecond = lblEmailSecond.Text;
            string Operator = lblOperator.Text;

            PopulateUploadedFilesList();

            EmailTemplate et = new EmailTemplate(EmailID, Convert.ToInt32(RecordID));
            et.BodyText = reBody.Content;
            et.UploadedFiles = UploadedFiles;
            et.PrepareMailMessage((EmailSecond == "1"));
            et.Operator = Operator;

            et.SendEmail();

            if (et.Sent)
            {
                reBody.Visible = false;
                btnSend.Visible = false;
                litFirstname.Visible = false;
                AsyncUpload.Visible = false;
                lblAttachments.Visible = false;
                litMessage.Visible = true;
                litMessage.Text = "<h3>Email sent.</h3>";
                btnCancel.Text = "close";

            }
            else
            {
                litMessage.Visible = true;
                litMessage.Text = "<div style=\"font-size:16px;color:red\">Error.</div>";
            }
        }

        private void PopulateUploadedFilesList()
        {
            foreach (UploadedFile file in AsyncUpload.UploadedFiles)
            {
                try
                {
                    UploadedFileInfo uploadedFileInfo = new UploadedFileInfo(file);
                    UploadedFiles.Add(uploadedFileInfo);
                }
                catch { throw new Exception("Something went wrong with attaching files. The file is:" + file.FileName); }
            }
        }
    }
}