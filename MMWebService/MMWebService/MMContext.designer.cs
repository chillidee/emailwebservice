﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MMWebService
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="CRISMAR")]
	public partial class MMContextDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertPCodeLL(PCodeLL instance);
    partial void UpdatePCodeLL(PCodeLL instance);
    partial void DeletePCodeLL(PCodeLL instance);
    #endregion
		
		public MMContextDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["MMConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public MMContextDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MMContextDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MMContextDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MMContextDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<PCodeLL> PCodeLLs
		{
			get
			{
				return this.GetTable<PCodeLL>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.PCodeLL")]
	public partial class PCodeLL : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _PCodeID;
		
		private string _SUBURB;
		
		private string _PCODE;
		
		private string _STATE;
		
		private string _TVSubMarket;
		
		private string _SubMarket;
		
		private System.Nullable<double> _Lat;
		
		private System.Nullable<double> _Lon;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnPCodeIDChanging(int value);
    partial void OnPCodeIDChanged();
    partial void OnSUBURBChanging(string value);
    partial void OnSUBURBChanged();
    partial void OnPCODEChanging(string value);
    partial void OnPCODEChanged();
    partial void OnSTATEChanging(string value);
    partial void OnSTATEChanged();
    partial void OnTVSubMarketChanging(string value);
    partial void OnTVSubMarketChanged();
    partial void OnSubMarketChanging(string value);
    partial void OnSubMarketChanged();
    partial void OnLatChanging(System.Nullable<double> value);
    partial void OnLatChanged();
    partial void OnLonChanging(System.Nullable<double> value);
    partial void OnLonChanged();
    #endregion
		
		public PCodeLL()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PCodeID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int PCodeID
		{
			get
			{
				return this._PCodeID;
			}
			set
			{
				if ((this._PCodeID != value))
				{
					this.OnPCodeIDChanging(value);
					this.SendPropertyChanging();
					this._PCodeID = value;
					this.SendPropertyChanged("PCodeID");
					this.OnPCodeIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SUBURB", DbType="NVarChar(50)")]
		public string SUBURB
		{
			get
			{
				return this._SUBURB;
			}
			set
			{
				if ((this._SUBURB != value))
				{
					this.OnSUBURBChanging(value);
					this.SendPropertyChanging();
					this._SUBURB = value;
					this.SendPropertyChanged("SUBURB");
					this.OnSUBURBChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PCODE", DbType="NVarChar(10)")]
		public string PCODE
		{
			get
			{
				return this._PCODE;
			}
			set
			{
				if ((this._PCODE != value))
				{
					this.OnPCODEChanging(value);
					this.SendPropertyChanging();
					this._PCODE = value;
					this.SendPropertyChanged("PCODE");
					this.OnPCODEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_STATE", DbType="NVarChar(10)")]
		public string STATE
		{
			get
			{
				return this._STATE;
			}
			set
			{
				if ((this._STATE != value))
				{
					this.OnSTATEChanging(value);
					this.SendPropertyChanging();
					this._STATE = value;
					this.SendPropertyChanged("STATE");
					this.OnSTATEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_TVSubMarket", DbType="VarChar(40)")]
		public string TVSubMarket
		{
			get
			{
				return this._TVSubMarket;
			}
			set
			{
				if ((this._TVSubMarket != value))
				{
					this.OnTVSubMarketChanging(value);
					this.SendPropertyChanging();
					this._TVSubMarket = value;
					this.SendPropertyChanged("TVSubMarket");
					this.OnTVSubMarketChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SubMarket", DbType="VarChar(40)")]
		public string SubMarket
		{
			get
			{
				return this._SubMarket;
			}
			set
			{
				if ((this._SubMarket != value))
				{
					this.OnSubMarketChanging(value);
					this.SendPropertyChanging();
					this._SubMarket = value;
					this.SendPropertyChanged("SubMarket");
					this.OnSubMarketChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Lat", DbType="Float")]
		public System.Nullable<double> Lat
		{
			get
			{
				return this._Lat;
			}
			set
			{
				if ((this._Lat != value))
				{
					this.OnLatChanging(value);
					this.SendPropertyChanging();
					this._Lat = value;
					this.SendPropertyChanged("Lat");
					this.OnLatChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Lon", DbType="Float")]
		public System.Nullable<double> Lon
		{
			get
			{
				return this._Lon;
			}
			set
			{
				if ((this._Lon != value))
				{
					this.OnLonChanging(value);
					this.SendPropertyChanging();
					this._Lon = value;
					this.SendPropertyChanged("Lon");
					this.OnLonChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
