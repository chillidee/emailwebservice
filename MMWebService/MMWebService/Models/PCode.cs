﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MMWebService.Models
{
	public class PCode
	{
		public int PID { get; set; }
		public string Sub { get; set; }
		public string PC { get; set; }
		public string St { get; set; }
		//public System.Nullable<double> Lat { get; set; }
		//public System.Nullable<double> Lon { get; set; }
	}
}