﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mvc.Jsonp;
using System.Web.Mvc;


namespace MMWebService.Controllers
{
		
    /*public class PCodeController : JsonpControllerBase
    {
			private MMContextDataContext _context = new MMContextDataContext();
			public List<Models.PCode> Index( string callback = "myCallbackFunction") 
			{
				var pCodeLLs = from p in _context.PCodeLLs
											 where p.SUBURB == "GEELONG"
											 select new Models.PCode { PID = p.PCodeID, Sub = p.SUBURB, PC = p.PCODE, St = p.STATE
												 //Lat = p.Lat, Lon = p.Lon
											 };
				return pCodeLLs.ToList();
				//return this.Jsonp(pCodeLLs, callback);
			}
			
		}*/
	public class PCodeController : ApiController
	{
		private MMContextDataContext _context = new MMContextDataContext();
		public List<Models.PCode> Get()
		{
			var pCodeLLs = from p in _context.PCodeLLs
										 where p.SUBURB == "GEELONG"
										 select new Models.PCode
										 {
											 PID = p.PCodeID,
											 Sub = p.SUBURB,
											 PC = p.PCODE,
											 St = p.STATE
											 //Lat = p.Lat, Lon = p.Lon
										 };
			return pCodeLLs.ToList();
			//return this.Jsonp(pCodeLLs, callback);
		}

	}
}
